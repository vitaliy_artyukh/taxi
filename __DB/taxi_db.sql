-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 14 2015 г., 06:34
-- Версия сервера: 5.5.24-log
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `taxi_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  `extension` varchar(5) NOT NULL,
  `size` int(10) NOT NULL,
  `hash` varchar(64) NOT NULL,
  `date_create` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `name`, `extension`, `size`, `hash`, `date_create`) VALUES
(12, '1435146213_email.png', 'png', 2491, '3b44034b05af174d9bc954173f253ee1', 1436514529),
(20, '1435023622_sign-info.png', 'png', 3871, '9a1e4cc8c6cf3b6b18022d8bb6910dd4', 1436515848),
(21, '234_taxi-floydworx_1x.png', 'png', 57682, '84b8696db1c395655d572fa6ec3af774', 1436517513),
(22, '1434981112_github.png', 'png', 6705, 'bf3c3d711f89878b0521c41e5633e491', 1436517661),
(23, '1431051078_supportmale-128.png', 'png', 8630, '15143707f97684a81e15adac4b1d52c5', 1436517687),
(24, '1435112060_file.png', 'png', 3693, '0934e30914aa9317f28c2bfc2bc217e2', 1436526290);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
