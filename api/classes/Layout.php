<?
    /**
    ** общие свойства и методы
    **/

    class Layout extends Std
    {        
        /**
        * Множественное обновление
        * @param $table - таблица сортировки
        * @param $data - данные для обновления
        * @param $condition - условия обновления
        **/
        
        public function putUpdate($table, $data, $condition = array())
        {
            $where = array();
            $records = array();
            foreach($data as $key => $value)
            {
                $records[] = "`$key` = " . Db::st()->escapeString($value);
            }
            foreach($condition as $field => $value)
            {
                $where[] = "`$field` = ".Db::st()->escapeString($value);
            }
            $update = Db::st()->update("
                UPDATE `?p` SET ?p WHERE ?p
            ", $table, implode(', ', $records), implode(', ', $where));
            if ($update)
            {
                return array(ERR => SUCCESS, MSG => 'Данные успешно обновлены');
            }
            else
            {
                return array(ERR => INTERNAL, MSG => SQL_ERROR);
            }
        }
        
        /**
        * удаление обьекта
        * @param $table - имя таблица
        **/
        
        public function deleteItem($data, $table, $limit = 1)
        {
            $delete = Db::st()->delete("DELETE FROM `?p` WHERE `id` = ?i LIMIT ?i", $table, $data['id'], $limit);
            if($delete)
            {
                return array(ERR => SUCCESS, MSG => 'Обьект успешно удален');
            }
            else
            {
                return array(ERR => INTERNAL, MSG => SQL_ERROR);
            }
        }
        
        public function page_404()
        {
            Tpl::st()->display('template_view', '404_view');
        }
    }
?>