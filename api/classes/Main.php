<?
    class Main extends Std
    {
        public function _routing()
        {
            Http::st()->route('/', 'main_page', __CLASS__);
        }
        
        public function main_page()
        {
            $data = array(
                'allow_extension' => Upload::st()->getAllowExtension(), 
                'max_upload_size' => Upload::st()->getMaxUploadSize(),
                'time' => date('U')
            );
            Tpl::st()->set('data', $data);
            Tpl::st()->set('files', Upload::st()->getFiles());
            Tpl::st()->display('template_view', 'main_view');
        }
    }
?>