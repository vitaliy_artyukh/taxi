<?
    /**
    * Все данные файла (название, размер, время загрузки и тд) хранятся в БД. 
    * Файл уникально идентифицируется по некому id. 
    * Физически файлы хранятся в файловой системе на сервере. 
    * В классе должны быть реализованы методы upload — загрузка файлов, принимает аргументом элемент массива $_FILES, возвращает результатом ошибку или id загруженного файла, info -- принимает id файла и возвращает всю информацию по нему, включая полный путь на сервере. В классе настраивается upload_dir —  директория загрузки файлов, допустимые расширения и максимальный размер загружаемого файла. Просьба не использовать PDO и т.п.
    * 
    * Задача JS: сделать часы. Требования: 

Часы могут быть цифровые, то есть просто вывод текстом времени, например: "06.10.2014 22:32". Часы должны инициализироваться серверным временем (формат на ваше усмотрение), а так же должен быть метод обновления времени со стороны сервера (обмен между сервером и клиентом реализовывать не надо). Они должны выводить текущее время сервера, вне зависимости от локальных настроек. Так же должна существовать возможность смены часового пояса на клиентской стороне путем выбора его из списка. Если будет время - хотелось бы еще функцию таймера обратного отсчёта с выводом сколько осталось часов, минут и секунд. Для селекторов можно использовать jquery
    **/
    
    class Upload extends Std
    {           
        private static $upload_dir = '/upload/';
        private static $allow_ext = array('jpg', 'png', 'bmp', 'gif');
        private static $max_upload_size = 11; //Mb
        private static $max_img_width = 2000;
        
        public function getAllowExtension()
        {
            return self::$allow_ext;
        }
        
        public function getMaxUploadSize()
        {
            return self::$max_upload_size;
        }
        
        /**
        * Получить список всех заметок
        **/
        
        public function getFiles()
        {
            $rows = Db::st()->rows("SELECT `id`, `name`, `extension`, `size`, `date_create` FROM `files` ORDER BY `date_create` DESC");
            if($rows)
            {
                foreach($rows as &$row)
                {
                    $row['path'] = self::$upload_dir.$row['name'];
                    $row['size'] = File::st()->formatBytes($row['size']);
                    $row['date_create'] = date('d.m.Y H:i', $row['date_create']);
                }
            }
            return $rows;
        }
        
        /**
        * получить информацию о файле
        * @id - id файла из таблицы files
        * @return array()
        **/
        
        public function getFileInfo($id)
        {
            $row = Db::st()->row("SELECT `id`, `name`, `extension`, `size`, `date_create` FROM `files` WHERE `id` = ?i", (int)$id);
            if(!$row)
            {
                return array(ERR => PREFAIL, MSG => 'undefined id file');
            }
            if(!is_file(DOCUMENT_ROOT.self::$upload_dir.$row['name']))
            {
                return array(ERR => PREFAIL, MSG => 'Файл не найден');
            }
            $row['path'] = self::$upload_dir.$row['name'];
            $row['realpath'] = DOCUMENT_ROOT.$row['path'];
            $row['size'] = File::st()->formatBytes($row['size']);
            $row['date_create'] = date('d.m.Y H:i', $row['date_create']);
            return $row;
        }
        
        /**
        * Загрузка файлов
        * $data - принимает параметры от загрузчика (если есть)
        **/
        
        public function postUpload($data)
        {
            $temp_hash = hash_file('md5', $_FILES['files']['tmp_name'][0]);
            if(self::checkExistHash($temp_hash))
            {
                return array(ERR => PREFAIL, MSG => 'Файл уже загружен/существует');
            }
            
            $info = Uploader::st()
                ->setInputName('files')
                ->originalFileName(true)
                ->setImageWidth(self::$max_img_width)
                ->setSizeLimit(1048576 * self::$max_upload_size)
                ->setAllowedExtensions(self::$allow_ext)
                ->setUploadDir(DOCUMENT_ROOT.self::$upload_dir)
                ->upload();
            
            $info = $info[0];
            if($info['errors'])
            {
                return array(ERR => PREFAIL, MSG => $info['errors'][0]);
            }            
            $post = Db::st()->insert("
                INSERT INTO `files` (`name`, `extension`, `size`, `hash`, `date_create`) VALUES (?s, ?s, ?i, ?s, ?i) 
            ", $info['basename'], $info['extension'], (int)$info['size'], $temp_hash, time());
            if($post)
            {
                $file = self::getFileInfo($post);
                return array(ERR => SUCCESS, MSG => 'Файл успешно загружен', DATA => $file);
            }
            return array(ERR => PREFAIL, MSG => SQL_ERROR);
        }
        
        /**
        * проверка на существование файла
        * @param $hash - string
        * @return boolean
        **/
        
        private function checkExistHash($hash)
        {
            $data = Db::st()->cell("SELECT `hash` FROM `files` WHERE `hash` = ?s", $hash);
            if($data)
            {
                return true;
            }
            return false;
        }
        
        /**
        * удаление файля
        * @param $data - array()
        **/
        
        public function deleteFile($data)
        {
            if(!$data['id'])
            {
                return array(ERR => PREFAIL, MSG => "undefined id");
            }
            $file = self::getFileInfo($data['id']);
            unlink(DOCUMENT_ROOT.self::$upload_dir.$file['name']);
            return Layout::st()->deleteItem($data, 'files');
        }
        
        public function putTimezome($data)
        {
            date_default_timezone_set($data['timezone']);
            return array(ERR => SUCCESS, MSG => 'Часовой пояс изменен: '.$data['timezone'], 'time' => date('U') + date('Z'));
        }
    }
?>