<?	
    if(!date_default_timezone_get())
	{
		date_default_timezone_set('Ukraine/Kiev');
	}
    
    /**
    ** General
    **/
    
    ini_set("display_errors", 1);
    ini_set("log_errors", 1);
    error_reporting(E_ALL & ~E_STRICT);
    
    define('HOST', $_SERVER['HTTP_HOST']);
    define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);
    define('DIRECTORY_INDEX', '');
	
	define('D_S', DIRECTORY_SEPARATOR);
	define('DIR_ROOT', dirname(__FILE__) . D_S);
	define('DIR_CORE', DIR_ROOT . 'core' . D_S);
	define('DIR_CLASSES', DIR_ROOT . 'classes' . D_S);
	define('DIR_INCLUDES', DIR_ROOT . 'includes' . D_S);
    define('DIR_VIEWS', dirname(DIR_ROOT) . D_S . 'views' . D_S);
    define('DIR_CSS', dirname(DIR_ROOT) . D_S . 'css' . D_S);
    define('DIR_IMG', dirname(DIR_ROOT) . D_S . 'img' . D_S);
    define('DIR_JS', dirname(DIR_ROOT) . D_S . 'js' . D_S);
    
    define('PHP_EXT', '.php');
    define('PAGE_EXT', '.php');
    
    define('POWERED_BY', 'Heart');
	define('CORE_SESSION_NAME', 'heartid');
    define('CORE_VERSION', '2.7.2');
    
	/**
    ** MYSQL
    **/

	define('DB_HOST', 'localhost');
    define('DB_NAME', 'taxi_db');
	define('DB_USER', 'root');
	define('DB_PASS', '');
	
    /**
    ** Core defines
    **/
    
	define('INDEX_METHOD', 'index');
	define('NOTFOUND_CLASS', 'Notfound');
	define('DEBUG_MODE', true); //show debug forced
    define('DEBUG_MODE_QUERY', true); //show debug query
    define('CACHE_MODE', false);
	
    /**
    ** Api response defines
    **/
    
	define('ERR', 'error');
	define('MSG', 'message');
    define('DATA', 'data');
    define('HTML', 'html');
	define('IFC', 'interface');
	define('DBG', 'debug');
	define('ACC', 'account');
	define('FRM', 'field');
    define('STATUS', 'status');
    define('FILE_DATA', 'file');
    define('LINE', 'line');
    define('TRACE', 'trace');
    
    /**
    ** Rest header defines
    **/
    
    define('NOLIMIT', 0);
	define('OK', 200);
	define('SUCCESS', 200);
	define('CREATED', 201);
	define('FOUNDED', 302);
	define('BAD', 400);
	define('BADREQUEST', 400);
	define('FORBIDDEN', 403);
	define('NOTFOUND', 404);
	define('NOTALLOW', 405);
	define('TIMEOUT', 408);
	define('PREFAIL', 412);
	define('INTERNAL', 500);
	define('IMPLEMENTED', 501);
	define('BADGATEWAY', 502);
	define('UNAVAILABLE', 503);
	define('UNAVAILABLE_TIMEOUT', 3600);
	
    /**
    ** Event units
    **/
    
	define('_HEART_', 'Heart');
	define('_DATAS_', 'Datas');
	define('_EVENT_', 'Event');
	define('_DB_', 'Db');
	define('_STD_', 'Std');
	
    /**
    ** Events list
    **/
    
    define('BEAT', 'beat');
	define('INIT', 'init');
	define('PREOUT', 'before_output');
	define('POSTOUT', 'after_output');
	define('ROUTER', 'router');
	define('SANK', 'sank'); //when catch exceptions (503)
	define('HELPER', 'helper'); //when sending Allow: methods_list, REST require
    
    /**
    ** Custom constants
    **/
    
    define('SQL_ERROR', 'Ошибка изменения данных или данные небыли изменены!');
    define('INTERNAL_ERROR', 'Внутренняя ошибка');
    
    /**
    ** Include additional functions
    **/
    
	require_once(DIR_ROOT . 'functions.php');
    
    /**
    * состояние PHP firebug
    **/
    
    FB::setEnabled(DEBUG_MODE);
	
    /**
    ** Debugging Php errors through browser console
    **/
    
	function portalNoticeWarningHandler($errno = '', $errstr = '', $errfile = '', $errline = '')
	{
        FB::error('PHP Notice or Warning: ' . $errfile . ' on line ' . $errline . ' ' . $errstr);
	}
	set_error_handler('portalNoticeWarningHandler', E_NOTICE | E_WARNING);
	
    /**
    ** Init once event sub-system
    **/
    
	Event::st()->init_once();
?>
