<?php
    
    /**
    ** класс для вывода и преобразования данных
    **/
    
    class Datas extends Std
    {
        // глобальный массив $_GET
        public $get = array();
        // глобальный массив $_POST
        public $post = array();
        // глобальный массив $_INPUT (попадаю данные переданные методом PUT | DELETE)
        private $input = array();
        // глобальный массив $_SESSION
        private $session = array();
        protected $class_name = "Datas";
        private $headers = array('X-Powered-By' => POWERED_BY);
        // массив с кодами ошибок
        private $statuses = array(
            //success
            200 => '200 OK',
            201 => '201 Created',
            
            //errors
            400 => '400 Bad Request',
            403 => '403 Forbidden',
            404 => '404 Not Found',
            405 => '405 Method Not Allowed',
            408 => '408 Request Timeout',
            412 => '412 Precondition Failed',
            
            //internal errors
            500 => '500 Internal Server Error',
            501 => '501 Not Implemented',
            502 => '502 Bad Gateway',
            503 => '503 Service Unavailable'
        );
        
        /**
        ** записываем данные в глобальные массивы
        **/
        
        public function __construct()
        {
            ini_set('session.name', CORE_SESSION_NAME);
            session_start();
            $this->session = $_SESSION; //$this->esc($_SESSION);
            $this->cookie = $_COOKIE; //$this->esc($_SESSION);
            $this->get = $this->esc($_GET);
            $this->post = $this->esc($_POST);
            $input = file_get_contents('php://input');
            parse_str($input, $_PUT);
            $this->input = $this->esc($_PUT);
            $this->id = $_SERVER['REQUEST_URI'];
        }
        
        public function status($number)
        {
            if(isset($this->statuses[$number]))
            {
                $this->headers["Status"] = $this->statuses[$number];
                $this->headers[] = "HTTP/1.0 " . $this->statuses[$number];
            }
        }
        
        public function cache()
        {
            $this->headers["Cache-Control"] = "no-store, no-cache, must-revalidate";
            $this->headers["Expires"] = date("r");
        }
        
        public function header($key, $val)
        {
            if(empty($val))
            {
                $this->header[] = $key;
            }
            else
            {
                $this->header[$key] = $val;
            }
        }
        
        private function sendHeaders()
        {
            foreach($this->headers as $name => $value)
            {
                if(is_numeric($name))
                {
                    header($value);
                }
                else
                {
                    header($name . ": " . $value);
                }
            }
        }
        
        /**
        ** вывод данных в формате (если данные php => array(), ajax => json)
        **/
        
        public function out($data = array())
        {
            if(!is_array($data) || empty($data[ERR]))
            {
                $data = array(ERR => OK, MSG => $data);
            }
            $this->status($data[ERR]);
            $this->cache();
            $this->sendHeaders();
            $data = Event::st()->trigger(_DATAS_, PREOUT, $data, true);
            FB::info('Memory usage: ' . memory_usage() . '; Execution time: ' . execution_time());
            //change error to 0 if status < 400 ( BAD ), this need for correctly work js REST call
            if($data[ERR] < BAD)
            {
                $data[ERR] = 0;
            }
            
            switch($this->ext())
            {
                case 'xml':
					header('Content-Type: text/xml; charset=utf-8');
                    $output = '<?xml version="1.0" encoding="utf8"?>' . 
						"\n" . xml_encode($data);
                break;
                case 'txt':
                    $output = (string)($data['message']);
                break;
                case 'json': default:
					header("Content-Type: application/json;charset=utf-8");
                    $output = json_encode($data);
                break;
				case 'flexigrid':
					header("Content-Type: application/json;charset=utf-8");
					if(is_array($data['message']['rows']))
					{
						$output = json_encode($data['message']);
					}
					else
					{
						$output = json_encode($data);
					}
                break;
                case 'jsonp':
					if(!$this->get('callback'))
					{
						header('Content-Type: plain/text; charset=utf-8');
						$output = "Please specify variable: /?callback=exampleFunc' \n
							where exampleFunc = funcName(obj) {}";
					}
					else
					{
						header('Content-Type: text/javascript; charset=utf-8');
						$output = $this->get('callback') . '(' . json_encode($data) . ');';
					}
                break;
                /*case 'name':
					header('Content-Type: text/html; charset=utf-8');
					$output = '<script type="text/javascript">window.name="' . addslashes(json_encode($data)) . '";</script>';
                break;*/
            }
            echo $output;
            Event::st()->trigger(_DATAS_, POSTOUT, $data);
        }
        
        public function ext()
        {
			$parts = explode('.', basename($_SERVER['REQUEST_URI']));
			$ext = explode('?', strtolower($parts[count($parts) - 1]));
			return $ext[0];
        }
        
        /**
        ** экранирование данных
        **/
        
        public function esc($data = array())
        {
            if(count($data))
            {
                foreach($data as $key => $val)
                {
                    if(is_array($val))
                    {
                        $data[$key] = $this->esc($data[$key]);
                    }
                    else
                    {
                        $data[$key] = $this->e($data[$key]);
                    }
                }
            }
            return $data;
        }
        
        /**
        ** экранирование одной строки входных данных
        **/
        
        public function e($string = '')
        {
            return trim(htmlspecialchars($string));
        }
        
        /**
        ** возвращает url адресной строки разбитый в массиве (без учета GET параметров)
        **/
        
        public function uri()
        {
            $_SERVER['PATH_INFO'] = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : preg_replace('/^([^?]+)(\?.*?)?(#.*)?$/', '$1$3', $_SERVER['REQUEST_URI']);
            return explode('/', trim($_SERVER['PATH_INFO'], '/'));
        }
        
        /**
        ** возвращает метод запроса ( GET | POST )
        **/
        
        public function method()
        {
            return $_SERVER['REQUEST_METHOD'];
        }
        
        public function request_uri()
        {
            return $_SERVER['REQUEST_URI'];
        }
        
        public function uploadInfo($source, $param = 'name')
        {
            if(!empty($_FILES) && !empty($_FILES[$source]))
            {
                return $_FILES[$source][$param];
            }
        }
        
        /**
        ** загрузка данных (примитивная)
        **/
        
        public function upload($source, $target, $name = null)
        {
            if(!empty($_FILES) && !empty($_FILES[$source]['tmp_name']))
            {
                $dest = '/' . trim($target, '/') . D_S . ($name ? $name : $_FILES[$source]['name']);
                if(move_uploaded_file($_FILES[$source]['tmp_name'], $dest))
                {
                    return $dest;
                }
                return false;
            }
            return false;
        }
        
        /**
        ** получаем | записываем данные в глобальный массыв $_GET
        **/
        
        public function get($a = '', $b = '')
        {
            if(empty($a))
            {
                return $this->get;
            }
            else if(empty($b) && ($b!==null))
            {
                if(empty($this->get[$a]))
                {
                    return null;
                }
                else
                {
                    return $this->get[$a];
                }
            }
            else if($b === null)
            {
                unset($this->get[$a]);
            }
            else
            {
                $this->get[$a] = $b;
            }
        }
        
        public function post($a = '', $b = '')
        {
            if(empty($a))
            {
                return $this->post;
            }
            else if(empty($b) && ($b!==null))
            {
                if(empty($this->post[$a]))
                {
                    return null;
                }
                else
                {
                    return $this->post[$a];
                }
            }
            else if($b === null)
            {
                unset($this->post[$a]);
            }
            else
            {
                $this->post[$a] = $b;
            }
        }
        
        public function put($a = '', $b = '')
        {
            if(empty($a))
            {
                return $this->input;
            }
            else if(empty($b) && ($b!==null))
            {
                if(empty($this->input[$a]))
                {
                    return null;
                }
                else
                {
                    return $this->input[$a];
                }
            }
            else if($b === null)
            {
                unset($this->input[$a]);
            }
            else
            {
                $this->input[$a] = $b;
            }
        }
        
        public function delete($a = '', $b = '')
        {
            return $this->put($a, $b);
        }
        
        public function session($a = '', $b = '')
        {
            if(empty($a))
            {
                return $this->session;
            }
            else if(empty($b) && ($b!==null))
            {
                if(empty($this->session[$a]))
                {
                    return null;
                }
                else
                {
                    return $this->session[$a];
                }
            }
            else if($b === null)
            {
                unset($this->session[$a]);
                $_SESSION[$a] = null;
                unset($_SESSION[$a]);
				FB::warn('UNSET _SESSION ' . $a . $b . $_SESSION);
            }
            else
            {
                $_SESSION[$a] = $b;
                $this->session[$a] = $b;
            }
        }
        
        /**
        ** получаем | записываем данные в глобальный массыв $_COOKIE
        **/
        
        public function cookie($a = '', $b = '')
        {
            if(empty($a))
            {
                return $this->cookie;
            }
            else if(empty($b) && ($b!==null))
            {
                if(empty($this->cookie[$a]))
                {
                    return null;
                }
                else
                {
                    return $this->cookie[$a];
                }
            }
            else if($b === null)
            {
                unset($this->cookie[$a]);
                $_COOKIE[$a] = null;
                unset($_COOKIE[$a]);
            }
            else
            {
                $_COOKIE[$a] = $b;
                $this->cookie[$a] = $b;
            }
        }
    }
?>
