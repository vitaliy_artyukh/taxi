<?
    /**
    * @author vitaliy.artyukh
    * класс для работы с DB драйвер PDO
    * 
    * placeholders at the moment are:
    * ?s ("string") - strings (also DATE, FLOAT and DECIMAL)
    * ?i ("integer") - the name says it all
    * ?n ("name") - identifiers (table and field names)
    * ?a ("array") - complex placeholder for IN() operator  (substituted with string of 'a','b','c' format, without parentesis)
    * ?u ("update") - complex placeholder for SET operator (substituted with string of `field`='value',`field`='value' format)
    * ?p ("parsed") - special type placeholder, for inserting already parsed statements without any processing, to avoid double parsing.
    * ?v
    **/
    
    class Db extends Std
    {
        private static $functions = array(
            'cell', 'column', 'row', 'rows', 'insert', 'update', 'delete', 'keymap', 'keyval', 'tableFields', 'query'
        );
        
        private $option = array(
            // тип базы данных
            'db_type' => 'mysql',
            // кодировка базы
            'db_charset' => 'utf8',
            // пользователь базы данных
            'db_user' => DB_USER,
            // имя базы данных
            'db_name' => DB_NAME,
            // пароль к базе данных
            'db_pass' => DB_PASS,
            // хост базы данных
            'db_host' => DB_HOST,
            // вывод ошибок с подробной инфой
            'error_mode' => false
        );
        
        // ссылка подключения к базе
        private $link;
        
        public function __construct()
        {
            //$this->option = array_merge( $this->option, $opt );
            if( $this->option['error_mode'] )
            {
                set_exception_handler( array(__CLASS__, 'exception_handler') );
            }
            $this->link = $this->connect();
            $this->link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        
        /**
        * отлавливатель исключений
        * @return error message
        **/
        
        public static function exception_handler( $error )
        {
            $traces = $error->getTrace();
            $trace = array();
            foreach( $traces as $item_trace )
            {
                if( in_array($item_trace['function'], self::$functions) )
                {
                    $trace = $item_trace;
                }
            }
            $arguments = array_slice( $trace['args'], 1 );
            $args = '';
            if( $arguments )
            {
                foreach( $arguments as $arg => $val )
                {
                    if( is_array($val) )
                    {
                        $args .= 'array = (<br>';
                        foreach( $val as $key => $v )
                        {
                            $args .=   '   t'.$key.' => '.$v.'<br>';
                        }
                        $args .= ')<br>';
                    }
                    $args .= $arg . ' => ' . $val . '<br>';
                }
            }
            echo '<pre>'. $error->getMessage() .'<br>'. 'Error in file: '
                .$trace['file'] .' on line ' . $trace['line'] .'<br>'
                .'Query: '.$trace['args'][0] . '<br>'
                .'Arguments: <br></pre>';
            var_dump( $arguments );
        }
        
        /**
        * устанавливаем соединение
        * @return resource
        **/
        
        public function connect()
        {
            try
            {
                $link = new PDO($this->option['db_type'].':host='.$this->option['db_host'].';dbname='.$this->option['db_name'], $this->option['db_user'], $this->option['db_pass']);
                $link->exec("SET NAMES ".$this->option['db_charset']);
                return $link;
            }
            catch(PDOException $e)
            {
                echo $e->getMessage();
            }
        }
        
        /**
        * проверка соединения
        * @return boolean
        **/
        
        public function connected()
        {
            return (boolean)($this->link);
        }
        
        /**
        * получаем одну ячейку
        * @example Db::st()->cell('SELECT `first_name` FROM `users` WHERE `id_user` = ?i', 1)
        * @return string
        **/
        
        public function cell()
        {
            $query = $this->prepareQuery( func_get_args() );
            $start = microtime( true );
            $stmt = $this->link->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch();
            $timer = microtime( true ) - $start;
            $trace = $this->getCalledQuery();
            DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | RESULT: ['.$row[0].']' . ' | TIME: ' . $timer, 'CELL') : null;
            if( $row )
            {
                return $row[0];
            }
            return false;
        }
        
        /**
        * получаем один ряд
        * @example Db::st()->row(SELECT `id`, `first_name`, `email` FROM `users` WHERE `id_user` = ?i, 1)
        * @return array
        **/
        
        public function row()
        {
            $query = $this->prepareQuery( func_get_args() );
            $start = microtime( true );
            $res = $this->rawQuery( $query );
            $timer = microtime( true ) - $start;
            $trace = $this->getCalledQuery();
            DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | ROW ['.count($res).'] | TIME: ' . $timer, 'ROW') : null;
            if( $res )
            {
                return $res[0];
            }
            return false;
        }
        
        /**
        * получаем несколько строк
        * @example Db::st()->rows(SELECT `id`, `first_name`, `email` FROM `users` WHERE `id_user` IN (?a), array(1, 2, 3))
        * @return array
        **/
        
        public function rows()
        {
            $query = $this->prepareQuery( func_get_args() );
            $start = microtime( true );
            $res = $this->rawQuery( $query );
            $timer = microtime( true ) - $start;
            $trace = $this->getCalledQuery();
            DEBUG_MODE_QUERY ? FB::info($query . ' | '.$trace['file'].' on LINE: '.$trace['line'] . ' | ROWS_QUERY ['.count($res).']' . ' | TIME: ' . $timer, 'ROWS') : null;
            if( $res )
            {
                return $res;
            }
            return false;
        }
        
        /**
        * получаем данные по колонке
        * @example Db::st()->column(SELECT `first_name` FROM `users`)
        * @return array
        **/
        
        public function column()
        {
            $query = $this->prepareQuery( func_get_args() );
            $start = microtime( true );
            $res = $this->rawQuery( $query, PDO::FETCH_COLUMN );
            $timer = microtime( true ) - $start;
            $trace = $this->getCalledQuery();
            DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | TIME: ' . $timer, 'COLUMN') : null;
            if( $res )
            {
                return $res;
            }
            return false;
        }
        
        /**
        * получаем имена полей таблицы
        * @param $extend_mode - расширеный режим (показывает структуру таблицы)
        * @example Db::st()->tableFields('users')
        * @return array | asoc array
        **/
        
        public function tableFields($table, $extend_mode = false)
        {
            $query = "DESCRIBE `".$table."`";
            $start = microtime( true );
            $smtp = $this->link->prepare($query);
            $smtp->execute();
            $trace = $this->getCalledQuery();
            if( $extend_mode )
            {
                $res = $smtp->fetchAll( PDO::FETCH_ASSOC );
                array_walk($res, function(&$row)
                {
                    $row = array_change_key_case($row, CASE_LOWER);
                });
                $timer = microtime( true ) - $start;
                DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | TIME: ' . $timer, 'TABLE_FIELDS') : null;
                return $res;
            }
            else
            {
                $res = $smtp->fetchAll( PDO::FETCH_COLUMN );
                $timer = microtime( true ) - $start;
                DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | TIME: ' . $timer, 'TABLE_FIELDS') : null;
                return $res;
            }
        }
        
        /**
        * очистка таблицы
        * @example Db::st()->truncateTable('users')
        * @return bolean
        **/
        
        public function truncateTable($table)
        {
            $query = "TRUNCATE TABLE `".$table."`";
            $start = microtime( true );
            $smtp = $this->link->prepare($query);
            $res = $smtp->execute();
            $timer = microtime( true ) - $start;
            $trace = $this->getCalledQuery();
            DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | TIME: ' . $timer, 'DROP_TABLE') : null;
            if( $res )
            {
                return true;
            }
            return false;
        }
        
        /**
        * удаление таблицы
        * @example Db::st()->tableFields('users')
        * @return bolean
        **/
        
        public function dropTable($table)
        {
            $query = "DROP TABLE `".$table."`";
            $start = microtime( true );
            $smtp = $this->link->prepare($query);
            $res = $smtp->execute();
            $timer = microtime( true ) - $start;
            $trace = $this->getCalledQuery();
            DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | TIME: ' . $timer, 'DROP_TABLE') : null;
            if( $res )
            {
                return true;
            }
            return false;
        }
        
        /**
        * получаем ассоциативный масив, где первая колонка - ключ, a вторая - значение
        * @example Db::st()->keyval(SELECT `id`, `email` FROM `users`)
        * @return array
        **/
        
        public function keyval()
        {
            $query = $this->prepareQuery( func_get_args() );
            $start = microtime( true );
            $res = $this->rawQuery( $query, PDO::FETCH_KEY_PAIR );
            $timer = microtime( true ) - $start;
            $trace = $this->getCalledQuery();
            DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | TIME: ' . $timer, 'KEYVAL') : null;
            if( $res )
            {
                return $res;
            }
            return false;
        }
        
        /**
        * получаем ассоциативный масив, где первая колонка - ключ, a значением вся строка
        * @example Db::st()->keyval(SELECT `email`, `id`, `first_name` FROM `users`)
        * @return array
        **/
        
        public function keymap()
        {
            $query = $this->prepareQuery( func_get_args() );
            $start = microtime( true );
            $trace = $this->getCalledQuery();
            $res = $this->rawQuery( $query, PDO::FETCH_ASSOC|PDO::FETCH_GROUP );
            if( $res )
            {
                foreach( $res as $key => $value )
                {
                    $res[$key] = $value[0];
                }
                $timer = microtime( true ) - $start;
                DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | TIME: ' . $timer, 'KEYMAP') : null;
                return $res;
            }
            return false;
        }
        
        /**
        * запрос на вставку данных
        * @example Db::st()->insert('INSERT INTO `users` (`email`, `name`) VALUES (?s, ?s)', 'email', 'name');
        * @return lastInsertId
        **/
        
        public function insert()
        {
            $query = $this->prepareQuery( func_get_args() );
            $start = microtime( true );
            $stmt = $this->link->prepare( $query );
            $stmt->execute();
            $timer = microtime( true ) - $start;
            $trace = $this->getCalledQuery();
            DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | TIME: ' . $timer, 'INSERT') : null;
            return $this->link->lastInsertId();
        }
        
        /**
        * запрос на обновление данных
        * @example Db::st()->insert('UPDATE `users` SET `name`= ?s' WHERE `id` = ?i', 'name', 7);
        * @return countRow
        **/
        
        public function update()
        {
            $query = $this->prepareQuery( func_get_args() );
            $start = microtime( true );
            $stmt = $this->link->prepare( $query );
            $stmt->execute();
            $timer = microtime( true ) - $start;
            $trace = $this->getCalledQuery();
            DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | TIME: ' . $timer, 'UPDATE') : null;
            $result = $stmt->rowCount();
            return $result;
        }
        
        /**
        * запрос на удаление данных
        * @example Db::st()->insert('DELETE FROM `users` WHERE `id` = ?i', 7);
        * @return countRow
        **/
        
        public function delete()
        {
            $query = $this->prepareQuery( func_get_args() );
            $start = microtime( true );
            $stmt = $this->link->prepare( $query );
            $stmt->execute();
            $timer = microtime( true ) - $start;
            $trace = $this->getCalledQuery();
            DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | TIME: ' . $timer, 'DELETE') : null;
            $result = $stmt->rowCount();
            return $result;
        }
        
        /**
        * выполнение кастомного запроса
        * @example Db::st()->query('SHOW TABLES')
        * @return boolean
        **/
        
        public function query()
        {
            $query = $this->prepareQuery( func_get_args() );
            $start = microtime( true );
            $stmt = $this->link->prepare( $query );
            $res = $stmt->execute();
            $timer = microtime( true ) - $start;
            $trace = $this->getCalledQuery();
            DEBUG_MODE_QUERY ? FB::info($query . ' | FILE: ' . $trace['file'] . ' on LINE: ' . $trace['line'] . ' | TIME: ' . $timer, 'QUERY') : null;
            if( $res )
            {
                return true;
            }
            return false;
        }
        
        /**
        * возвращает ID последней вставленой записи
        * @return int
        **/
        
        public function last_id()
        {
            return $this->link->lastInsertId(); 
        }
        
        public function auto_increment($table)
        {
            return Db::st()->cell("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = ?s AND TABLE_NAME = ?s", DB_NAME, $table);
        }
        
        /**
        ** Инициализация транзакции
        **/
        
        public function startTransaction()
        {
            $this->link->beginTransaction();
        }
        
        /**
        ** запись транзакции
        **/
        
        public function commit()
        {
            $this->link->commit();
        }
        
        /**
        ** откат транзакции
        **/
        
        public function rollback()
        {
            $this->link->rollBack();
        }
        
        /**
        * подготовка запроса, замена placeholder на значения
        * @return string (запрос)
        **/
        
        public function prepareQuery( $args )
        {
            $query = '';
            $raw = array_shift($args);
            $array = preg_split('~(\?[nsiuvap])~u', $raw, null, PREG_SPLIT_DELIM_CAPTURE);
            $anum = count( $args );
            $pnum = floor(count($array) / 2);
            if( $pnum != $anum )
            {
                $this->error("Number of args ($anum) doesn't match number of placeholders ($pnum) in [$raw]");
            }
            $stmt = $this->link->prepare( $raw );
            foreach( $array as $i => $part )
            {
                if( ($i % 2) == 0 )
                {
                    $query .= $part;
                    continue;
                }
                $value = array_shift( $args );
                
                switch( $part )
                {
                    case '?n':
                        $part = $this->escapeIdent( $value );
                        break;
                    case '?s':
                        $part = $this->escapeString( $value );
                        break;
                    case '?i':
                        $part = $this->escapeInt( $value, $query );
                        break;
                    case '?a':
                        $part = $this->createIN( $value );
                        break;
                    case '?u':
                        $part = $this->createSET( $value );
                        break;
                    case '?v':
                        $part = $this->createVALUES( $value );
                        break;
                    case '?p':
                        $part = $value;
                        break;
                }
                $query .= $part;
            }
            return $query;
        }
        
        /**
        * екранирование строки, 
        **/
        
        public function escapeString($value)
        {
            if( $value === null )
            {
                return 'NULL';
            }
            return $this->link->quote($value);
        }
        
        /**
        * проверка на число
        * @return numeric: integer | string 
        **/
        
        public function escapeInt( $value, $query )
        {
            if ($value === null)
            {
                return 'NULL';
            }
            if(!is_numeric($value))
            {
                $this->error("Integer (?i) placeholder expects numeric value, ".gettype($value)." given " . $value . ' in query: ' . $query);
                return FALSE;
            }
            if (is_float($value))
            {
                $value = number_format($value, 0, '.', '');
            }
            return $value;
        }
        
        /**
        * построение строки из массива
        * @return string
        **/
        
        private function createIN( $data )
        {
            if( !is_array($data) )
            {
                $this->error("Value for IN (?a) placeholder should be array");
                return;
            }
            if( !$data )
            {
                return 'NULL';
            }
            foreach($data as &$item)
            {
                $item = $this->escapeString($item);
            }
            $query = implode(', ', $data);
            return $query;
        }
        
        /**
        * создание подготовленного массива
        * @param $data - входящий массив данных
        * @return string
        **/
        
        private function createSET( $data )
        {
            if( !is_array($data) )
            {
                $this->error("SET (?u) placeholder expects array, ".gettype($data)." given");
                return;
            }
            if( !$data )
            {
                $this->error("Empty array for SET (?u) placeholder");
                return;
            }
            $query = $comma = '';
            foreach( $data as $key => $value )
            {
                $query .= $comma.$this->escapeIdent($key).'='.$this->escapeString($value);
                $comma  = ",";
            }
            return $query;
        }
        
        /**
        * формирует строку вида: (3, 1), (4, 1), (5, 1) для вставки
        **/
        
        private function createVALUES( $data )
        {
            if( !is_array($data) )
            {
                $this->error("Value for VALUES (?v) placeholder should be array");
                return;
            }
            $query = '';
            $i = 0;
            foreach( $data as $key => $value )
            {
                $sep = $i !== 0 ? ", " : null;
                $query .= $sep . "('".implode("', '", $value)."')";
                $i++;
            }
            return $query;
        }
        
        /**
        * пропуск только разрешенных полей
        * @param $input - входные данные
        * @param $allowed - разрешенные данные
        * @example
        * $data = Db::st()->filterArray($_POST, array('name', 'login', 'pass'));
        * $db->query('INSERT INTO ?n SET ?u', $table, $data);
        * @return array
        **/
        
        public function filterArray($input, $allowed)
        {
            foreach( array_keys($input) as $key )
            {
                if( !in_array($key,$allowed) )
                {
                    unset( $input[$key] );
                }
            }
            return $input;
        }
        
        /**
        * выполнение запроса
        * @param $query - строка запроса
        * @param $fetch_mode - параметр, определяет, в каком виде PDO вернет строку:
        * PDO::FETCH_ASSOC - ассоциативный массив
        * PDO::FETCH_NUM - индексный массив
        * PDO::FETCH_BOTH - смешанным (индексы + именованные ключи)
        * PDO::FETCH_COLUMN - единственный запрашиваемый столбец, можно указать номер столбца
        * PDO::FETCH_GROUP - массив значений, сгруппированных по значению определённого столбца
        * PDO::FETCH_KEY_PAIR - массив, где 1-ый столбец - ключ второй является значением
        * @return data: string | array | array array
        **/
        
        private function rawQuery( $query, $fetch_mode = PDO::FETCH_ASSOC )
        {
            $start = microtime( true );
            $stmt = $this->link->prepare( $query );
            $stmt->execute();
            $rows = $stmt->fetchAll( $fetch_mode );
            $timer = microtime( true ) - $start;
            $this->stats[] = array(
                'query' => $query,
                'timer' => $timer
            );
            return $rows;
        }
        
        /**
        * @example Db::st()->getAttribute(PDO::ATTR_SERVER_VERSION) => 5.5.24-log
        * @return возвращает значение атрибута соединения с базой данных
        **/
        
        public function getAttribute( $attr )
        {
            return $this->link->getAttribute( $attr );
        }
        
        private function error($err)
        {
            $err = __CLASS__.": ".$err;
            if ( $this->option['error_mode'] )
            {
                $err .= ". Error initiated in ".$this->caller().", thrown";
                trigger_error($err,E_USER_ERROR);
            }
            else
            {
                throw new Exception($err);
            }
        }
        
        /**
        * обработчик ошибок
        * @return message: query, file, line
        **/
        
        public function caller()
        {
            $trace = debug_backtrace();
            $caller = '';
            foreach( $trace as $t )
            {
                if( isset($t['class']) && $t['class'] == __CLASS__ )
                {
                    $caller = $t['file']." on line ".$t['line'];
                }
                else
                {
                    break;
                }
            }
            echo $caller;
        }
        
        public function getCalledQuery()
        {
            $trace = debug_backtrace();
            foreach( $trace as $item_trace )
            {
                if( in_array($item_trace['function'], self::$functions) )
                {
                    return $item_trace;
                }
            }
        }
    }
?>