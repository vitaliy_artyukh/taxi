<?
    class Event extends Std
    {
        private $events = null;
        protected $class_name = "Event";
        
        //this function must be called once, after autoload declaration
        public function init_once()
        {
            if(is_array($this->events))
            {
                return true;
            }

            $this->events = array();
            $files = array_diff(array_merge(scandir(DIR_CORE), scandir(DIR_CLASSES)), array('Event.php', '.', '..'));
            foreach($files as $file)
            {
                $class = current(explode('.', $file));
                if(class_exists($class) && method_exists($class, '_ready'))
                {
                    $class::st()->_ready();
                }
            }
        }
        
        public function bind($unit, $event, $function)
        {
            if(empty($this->events[$unit]))
            {
                $this->events[$unit] = array();
            }
            if(empty($this->events[$unit][$event]))
            {
                $this->events[$unit][$event] = array();
            }
            $this->events[$unit][$event][] = $function;
            return $this;
        }
        
        public function unbind($unit, $event)
        {
            unset($this->events[$unit][$event]);
            return $this;
        }
        
        public function rebind($unit, $event, $function)
        {
            return $this->unbind($unit, $event)->bind($unit, $event, $function);
        }
        
        //be careful with using $returns
        public function trigger($unit, $event, $data = array(), $returns = false)
        {
            if($returns) //duplication, but the savings have
            {
                //FB::warn('Trigger with returns: ' . $unit . '; Event: ' . $event . '; Start Data: ' . $data);
                $result = null;
                if(isset($this->events[$unit][$event]))
                {
                    foreach($this->events[$unit][$event] as $one)
                    {
                        $result = call_user_func_array($one, array($data));
                        if(!empty($result) && is_array($result))
                        {
                            $data = $result;
                        }
                    }
                }
                //FB::warn('Trigger with returns: ' . $unit . '; Event: ' . $event . '; Finish Data: ' . $data);
                return $data;
            }
            else
            {
                //FB::log('Trigger: ' . $unit . '; Event: ' . $event . '; Data: ' . $data);
                $result = array();
                if(isset($this->events[$unit][$event]))
                {
                    foreach($this->events[$unit][$event] as $one)
                    {
                        $result[] = call_user_func_array($one, array($data));
                    }
                }
                return $result;
            }
        }
    }
?>