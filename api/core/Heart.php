<?
	/**
    ** Heart class for routing, contain only entry-points, such as:
    ** rest, payment-callbacks, cron-jobs, social-networking-calbacks
    **/
    
	class Heart extends Std
	{        
        /**
        ** сбор параметров для REST запроса
        **/
        
        public function rest()
		{
            $components = Datas::st()->uri();
            $components = $components;
			$class = ucfirst(strtolower((empty($components[0]) ? NOTFOUND_CLASS : $components[0])));
			$type = strtolower(Datas::st()->method());
			$methods = explode('.', ucfirst(strtolower((empty($components[1]) ? INDEX_METHOD : $components[1]))));
			$method = $type . $methods[0];
			$arguments = Datas::st()->$type();
			$params = array('class' => $class, 'method' => $method, 'arguments' => $arguments);
            if(isset($params['arguments']['PATH_INFO']))
            {
                $_SERVER['PATH_INFO'] = $params['arguments']['PATH_INFO'];
                unset($params['arguments']['PATH_INFO']);
            }
            return $this->call($params);
		}
        
        /**
        ** вызов метода из REST с передачей параметров
        **/

		public function call($params = '')
		{
			$params = Event::st()->trigger(_HEART_, ROUTER, $params, true);
			return $params['class']::st()->$params['method']($params['arguments']);
		}
    }
?>