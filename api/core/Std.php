<?
    /**
    * Singletone class
    */
    
    abstract class Std
    {
        private static $instances = array();
        
        public static function st($data = null)
        {
            $class = get_called_class();
            if(empty(self::$instances[$class]))
            {
                self::$instances[$class] = new $class($data);
            }
            if(method_exists(self::$instances[$class], '_setup'))
            {
                self::$instances[$class]->_setup($data);
            }
            return self::$instances[$class];
        }
        
        public function _helper()
        {
            //send allowed method of called class
            $c = get_called_class();
            $rest_methods = preg_grep("/^(get|post|put|delete)(.*)/i", get_class_methods($c));
            $rest_methods = Event::st()->trigger($c, HELPER, $rest_methods, true);
            $rest_methods = Event::st()->trigger(_STD_, HELPER, $rest_methods, true);
            if(count($rest_methods))
            {
                return $c . '->' . implode(', ' . $c . '->', $rest_methods);
            }
            else
            {
                return 'none';
            }
        }
        
        public function __call($m, $a)
        {
            $c = get_called_class();
            Datas::st()->header('Allow', $c::st()->_helper());
            return array('error' => NOTALLOW, 'message' => 'Method "' . $m . '" not allowed in ' . $c . ' class');
        }
    }
?>