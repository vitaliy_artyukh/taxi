<?
    /**
    ** класс шаблонизатора
    ** пример использования: 
    ** - передача данных Tpl::st()->set( 'data', $data );
    ** - вывод шаблона Tpl::st()->display( 'template_view', 'category_view' );
    **/
    
    class Tpl extends Std
    {
        // путь к папке с шаблонами
        protected $_path = DIR_VIEWS;
        // имя шаблона
        private $_template;
        // шаблон в кторый вставляем
        private $_content;
        // изменяемый шаблон header
        private $_header;
        // переменные передаваемые в шаблон
    	private $_var = array();
        // css
        private $_css = array();
        
        public function __construct()
        {
            $defaultPage = 'main';
            $this->get = Datas::st()->get;
            $this->uri = Datas::st()->uri();
            $this->id = end($this->uri);
            $this->action = count($this->uri) > 1 ? $this->uri[count($this->uri)-2] : null;
            $this->class = isset($this->uri[0]) ? $this->uri[0] : $defaultPage;
        }
        
        /**
        ** установка пути к шаблонам
        **/
        
        public function setTemlatePath($path)
    	{
    		$this->_path = $path;
    	}
        
        public function addCSS($path)
    	{
    		if( !in_array($path, $this->_css) )
            {
                $this->_css[] = $path;
            }
    	}
        
        /**
        ** установка переменной и ее значения
        **/
        
        public function set($name, $value)
    	{
    		$this->_var[$name] = $value;
    	}
        
        /**
        ** магический метод делает из несуществующей переменной -> переменную обьявленую сетом
        **/
        
        public function __get($name)
    	{
    		if( isset($this->_var[$name]) ) return $this->_var[$name];
    		return '';
    	}
        
        /**
        ** вывод шаблона
        **/
        
        public function display( $template, $content = null, $header = null, $strip = true )
    	{
            $this->_template = $this->_path . $template . PAGE_EXT;
            $this->_content = $content;
            $this->_header = $header;
    		if (!file_exists($this->_template)) die('Template ' . $this->_template . ' is not exist!');
    		ob_start();
    		include_once($this->_template);
    		echo ($strip) ? $this->_strip(ob_get_clean()) : ob_get_clean();
    	}
        
        /**
        ** подключение шаблона внутри другого шаблона
        ** пример использования:
        ** - статическое имя шаблона Tpl::st()->includeTpl( 'footer_view' );
        ** - динамическое имя шаблона Tpl::st()->includeTpl( Tpl::st()->_content ); Tpl::st()->includeTpl( Tpl::st()->_left );
        ** $_content - передается 2-м параметром в display()
        ** $_left - передается 2-м параметром в display()
        **/
        
        public function includeTpl( $tpl )
        {
            if( file_exists($this->_path . $tpl . PAGE_EXT) )
            {
                include( $this->_path . $tpl . PAGE_EXT );
            }
            else
            {
                echo 'error: template '.$tpl.' not found: ' . $this->_path . $tpl . PAGE_EXT;
            }
        }
        
        public function includeCSS()
        {
            $html = '';
            if($this->_css)
            {
                foreach($this->_css as $style)
                {
                    $html .= '<link rel="stylesheet" type="text/css" href="'.$style.'" />';
                }
            }
            return $html;
        }
        
        /**
        ** экранирование специальных символов
        **/
        
        private function _strip($data)
    	{
    		$lit = array("\\t", "\\n", "\\n\\r", "\\r\\n", "  ");
    		$sp = array('', '', '', '', '');
    		return str_replace($lit, $sp, $data);
    	}
        
        /**
        ** проверка на существование шаблона | имя шаблона
        **/
        
        public function exists($tpl = '')
        {
            if( file_exists($this->_path . $tpl . PAGE_EXT) )
            {
                return true;
            }
            return false;
        }
    }
?>
