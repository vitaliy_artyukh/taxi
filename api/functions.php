<?php
	
    /**
    ** подключение ядра, классов, дополнительных библиотек
    ** функции общего назначения и тд, которых нет в cборке php
    **/
    
    $GLOBALS['starttimer'] = microtime(true);
    
    // подключаем PHP firebug
    require_once( DIR_INCLUDES . 'FirePHPCore' . D_S . 'fb.php' );
        
    function __autoload($class)
	{
        if(file_exists(DIR_CORE . $class . '.php'))
		{
            require_once(DIR_CORE . $class . '.php');
		}
		else if(file_exists(DIR_CLASSES . $class . '.php'))
		{
            require_once(DIR_CLASSES . $class . '.php');
		}
		else if(file_exists(DIR_INCLUDES . $class . '.php'))
		{
            require_once(DIR_INCLUDES . $class . '.php');
		}
		else if(file_exists(DIR_INCLUDES . 'class.' . $class . '.php'))
		{
			require_once(DIR_INCLUDES . 'class.' . $class . '.php');
		}
        else
		{
            FB::warn('Autoload: class "' . $class . '" not found');
            throw new Exception('Internal error. Class "' . $class . '" not found.');
		}
	}

	function inc($file = '')
	{
		if(file_exists(DIR_INCLUDES . $file) && is_file(DIR_INCLUDES . $file))
		{
			return require_once(DIR_INCLUDES . $file);
		}
		return false;
	}
	
	function memory_usage()
	{
		$usage = memory_get_usage(true);
		if ($usage < 1024)
			return $usage . " bytes";
		elseif ($usage < 1048576)
			return round($usage / 1024, 2) . " Kb";
		else
			return round($usage / 1048576, 2) . " Mb";
	}

	function backtrace()
	{
		print_r('<pre>');
        print_r(debug_backtrace());
        print_r('</pre>');
	}
	
	function execution_time()
	{
		return (microtime(true) - $GLOBALS['starttimer']) * 100;
	}
    
    function is_main()
    {
        return $_SERVER['REQUEST_URI'] == '/' ? true : false;
    }
?>