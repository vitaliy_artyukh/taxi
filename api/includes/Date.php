<?
    /**
    * класс для работы с датой
    **/
    
    class Date extends Std
    {
        public $month = array(
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря'
        );
        
        /**
        * определение возраста по дате рождения
        **/
        
        public function getAgeByDate( $date )
        {
            $age = date('Y') - date('Y', strtotime($date));
            if( $age ) return $age;
        }
        
        /**
        * подсчет количества дней с момента регистрации
        **/
        
        public function getDayInSystem( $start_date )
        {
            $start_date = date_create( $start_date );
            $today_date = date_create( date('Y-m-d H:i:s') );
            $interval = date_diff( $start_date, $today_date );
            return $interval->days;
        }
        
        /**
        * форматирование даты
        **/
        
        function dateFormat($format, $date)
        {
            $format = str_replace('mmmm', $this->month[date('n', strtotime($date))], $format);
            $date_format = date($format, is_numeric($date) ? $date : strtotime($date));
            return $date_format;
        }
        
    }
?>