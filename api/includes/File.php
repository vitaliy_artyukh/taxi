<?
    class File extends Std
    {
        private static $temp = array();
        public static $mimeTypes = array(
            'asc'   => 'text/plain',
            'au'    => 'audio/basic',
            'avi'   => 'video/x-msvideo',
            'bin'   => 'application/octet-stream',
            'class' => 'application/octet-stream',
            'css'   => 'text/css',
            'csv' => 'application/vnd.ms-excel',
            'doc'   => 'application/msword',
            'docx'   => 'application/msword',
            'dll'   => 'application/octet-stream',
            'dvi'   => 'application/x-dvi',
            'exe'   => 'application/octet-stream',
            'htm'   => 'text/html',
            'html'  => 'text/html',
            'json'  => 'application/json',
            'js'    => 'application/x-javascript',
            'txt'   => 'text/plain',
            'bmp'   => 'image/bmp',
            'rss'   => 'application/rss+xml',
            'atom'  => 'application/atom+xml',
            'gif'   => 'image/gif',
            'jpeg'  => 'image/jpeg',
            'jpg'   => 'image/jpeg',
            'jpe'   => 'image/jpeg',
            'png'   => 'image/png',
            'psd'   => 'image/psd',
            'ico'   => 'image/vnd.microsoft.icon',
            'mpeg'  => 'video/mpeg',
            'mpg'   => 'video/mpeg',
            'mpe'   => 'video/mpeg',
            'qt'    => 'video/quicktime',
            'mov'   => 'video/quicktime',
            'wmv'   => 'video/x-ms-wmv',
            'mp2'   => 'audio/mpeg',
            'mp3'   => 'audio/mpeg',
            'rm'    => 'audio/x-pn-realaudio',
            'ram'   => 'audio/x-pn-realaudio',
            'rpm'   => 'audio/x-pn-realaudio-plugin',
            'ra'    => 'audio/x-realaudio',
            'wav'   => 'audio/x-wav',
            'zip'   => 'application/zip',
            'pdf'   => 'application/pdf',
            'xls'   => 'application/vnd.ms-excel',
            'ppt'   => 'application/vnd.ms-powerpoint',
            'wbxml' => 'application/vnd.wap.wbxml',
            'wmlc'  => 'application/vnd.wap.wmlc',
            'wmlsc' => 'application/vnd.wap.wmlscriptc',
            'spl'   => 'application/x-futuresplash',
            'gtar'  => 'application/x-gtar',
            'gzip'  => 'application/x-gzip',
            'swf'   => 'application/x-shockwave-flash',
            'tar'   => 'application/x-tar',
            'xhtml' => 'application/xhtml+xml',
            'snd'   => 'audio/basic',
            'midi'  => 'audio/midi',
            'mid'   => 'audio/midi',
            'm3u'   => 'audio/x-mpegurl',
            'tiff'  => 'image/tiff',
            'tif'   => 'image/tiff',
            'rtf'   => 'text/rtf',
            'wml'   => 'text/vnd.wap.wml',
            'wmls'  => 'text/vnd.wap.wmlscript',
            'xsl'   => 'text/xml',
            'xml'   => 'text/xml'
        );
        
        public function getMimeTypes()
        {
            return self::$mimeTypes;
        }
        
        public function getExtensions()
        {
            $result = array();
            $items = self::$mimeTypes;
            foreach($items as $key => $val)
            {
                $result[]['title'] = $key;
            }
            return $result;
        }
        
        public function getFileInfo($path)
        {            
            if(!is_file($path))
            {
                return false;
                //return array(ERR => INTERNAL, MSG => 'File not exist');
            }
            $info = array();
            $info = pathinfo($path);
            $info['file_path'] = str_replace(DOCUMENT_ROOT, '', $path);
            $info['file_dir'] = str_replace($info['filename'].'.'.$info['extension'], '', $info['file_path']);
            $info['file_size'] = filesize($path);
            $info['file_size_f'] = $this->formatBytes(filesize($path));
            return $info;
        }
        
        function formatBytes($bytes, $precision = 2)
        { 
            $units = array('B', 'KB', 'MB', 'GB', 'TB'); 
            $bytes = max($bytes, 0);
            $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
            $pow = min($pow, count($units) - 1);
            $bytes /= pow(1024, $pow); 
            return round($bytes, $precision) . ' ' . $units[$pow]; 
        }
        
        /**
        * удаление файла из папки
        * @param $file_dir - относительный путь папки с изображение
        * @example '/images/users/'
        * @param $file_name - имя файла (без расширения)
        **/
        
        public function deleteFile($file_dir, $file_name)
        {
            $file_dir = DOCUMENT_ROOT.$file_dir;
            if(is_dir($file_dir))
            {
                // сканируем директорию
                $files = scandir( $file_dir );
                unset($files[0], $files[1]);
                $i = 0;
                foreach($files as $file)
                {
                    if( strpos($file, $file_name) === 0 )
                    {
                        if( unlink( $file_dir.$file ) )
                        {
                            $i++;
                        }
                    }
                }
                if( $i > 0 )
                {
                    return array( ERR => SUCCESS, MSG => 'Удалено файлов: '.$i );
                }
                else
                {
                    return array( ERR => INTERNAL, MSG => 'Ошибка при удалении файла' );
                }
            }
        }
        
        function deleteDirectory($dirPath)
        {
            if (!is_dir($dirPath))
            {
                throw new Exception("$dirPath must be a directory");
            }
            if(substr($dirPath, strlen($dirPath) - 1, 1) != '/')
            {
                $dirPath .= '/';
            }
            $files = glob($dirPath . '*', GLOB_MARK);
            foreach($files as $file)
            {
                if(is_file($file))
                {
                    self::$temp['file']++;
                    unlink($file);
                }
                else
                {
                    self::$temp['dir']++;
                    self::deleteDirectory($file);
                }
            }
            rmdir($dirPath);
            return array(ERR => SUCCESS, MSG => self::$temp);
        }
    }
?>