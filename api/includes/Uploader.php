<?
    class Uploader extends Std
    {
        protected $_name; // имя input[type='file']
        protected $_file;
        protected $_allowedExtensions;
        protected $_sizeLimit = 1048576; // 1M
        protected $_originalFileName = false;
        protected $_fileName; // имя файла
        protected $_fileNameArray; // имена файлов при загрузке нескольких файлов
        protected $_uploadDir;
        protected $_relativeDir;
        protected $_isArray = false;
        
        protected $imageQuality = 100;
        protected $imageWidth = null;
        public $img_ext;
        
        protected $_imageExtensions = array('jpg', 'jpeg', 'png', 'gif', 'bmp');
        protected $_errors = array(
            'Файл (%s) уже существует',
            'Файл (%s) не выбран',
            'Файл (%s) слишком большой, максимально допустимый размер %s Mb',
            'Файл имеет недопустимое расширение. Файл должно быть с раширением: %s'
        );
        
        public function __construct($name)
        {
            /*
            var_dump( $this->_fileName );
            if( isset($_GET[$name]) )
            {
                $this->_file = new Q_Uploader_Method_Xhr($name);
            }
            elseif( isset($_FILES[$name]) )
            {
                $this->_file = $_FILES[$name];
                $this->_isArray = is_array($_FILES[$name]['name']);
            }
            else
            {
                throw new Exception('Could not find files for upload');
            }
            */
        }
        
        /**
        * устанавливаем имя input[type='file']
        **/
        
        public function setInputName( $name )
        {
            $this->_name = $name;
            $this->_file = $_FILES[$name];
            $this->_isArray = is_array($_FILES[$name]['name']);
            return $this;
        }
    
        /**
        * устанавливаем разрешенные расширения для загружаемых файлов
        * @param array $extensions
        **/
        
        public function setAllowedExtensions( array $extensions )
        {
            $this->_allowedExtensions = $extensions;
            return $this;
        }
    
        /**
        ** устанавливаем лимит загрузки файлов
        **/
        
        public function setSizeLimit( $size )
        {
            $this->_sizeLimit = $size;
            $this->checkServerSettings();
            return $this;
        }
        
        /**
        ** устанавливаем ширину изображения
        **/
        
        public function setImageWidth( $width )
        {
            $this->imageWidth = $width;
            return $this;
        }
    
        /**
        ** устанавливаем директорию для загрузки файлов
        **/
        
        public function setUploadDir( $dir )
        {
            $this->prepareDir( $dir );
            $this->_uploadDir = $dir;
            return $this;
        }
        
        /**
        ** устанавливаем директорию для загрузки файлов (относительную - для return)
        **/
        
        public function setRelativeDir( $dir )
        {
            $this->_relativeDir = $dir;
            return $this;
        }
    
        /**
        ** сохранять оригинальное имя файла или переименованное
        ** @param boolean $originalFileName
        **/
        
        public function originalFileName($originalFileName = false)
        {
            $this->_originalFileName = $originalFileName;
            return $this;
        }
    
        /**
        * устанвливаем имя файла
        * @param string $fileName
        **/
         
        public function setFileName($fileName)
        {
            $this->_fileName = $fileName;
            return $this;
        }
        
        /**
        * устанвливаем имена для нескольких файлов
        * @param string $fileNameArray
        **/
         
        public function setFileNameArray($fileNameArray)
        {
            $this->_fileNameArray = $fileNameArray;
            return $this;
        }
    
        /**
        * загружаем файл на сервер
        * @return array
        */
        
        public function upload()
        {
            if( !isset($_FILES[$this->_name]) )
            {
                throw new Exception('Could not find files for upload');
            }
            
            $originalNames = $this->getName();
            if( false === $this->_isArray ) $originalNames = array( $originalNames );
            
            $sizes = $this->getSize();
            if( false === $this->_isArray ) $sizes = array( $sizes );
            
            $return = array();
            foreach( $originalNames as $i => $originalName )
            {
                if( empty($originalName) ) continue;
                $size = $sizes[$i];
                $info = $this->uploadFile( $originalName, $size, $i );
                if( false === $this->_isArray ) return $info;
                $return[] = $info;
            }
            return $return;
        }
    
        /**
        * загрузка файлов
        */
        
        protected function uploadFile($originalName, $size, $i)
        {
            $pathinfo = pathinfo($originalName);
            $extension = strtolower($pathinfo['extension']);
            
            if( $this->_isArray && !isset($this->_fileNameArray[$i]) )
            {
                $errors[] = 'Ошибка при загрузке';
            }
            
            $filename = ($this->_fileName === null) ? ($this->_originalFileName === true ? $pathinfo['filename'] : md5(uniqid())) : $this->_fileName;
            $basename = $filename . '.' . $extension;
            $filePath = $this->_uploadDir . DIRECTORY_SEPARATOR . $basename;
            
            $errors = array();
    
            if (file_exists($filePath))
            {
                //$errors []= sprintf($this->_errors[0], $filePath);
            }
            if( 0 === $size )
            {
                $errors[] = sprintf($this->_errors[1], $originalName);
            }
            if( $size > $this->_sizeLimit)
            {
                $errors[] = sprintf($this->_errors[2], $originalName, $this->_sizeLimit / 1048576);
            }
            if( !empty($this->_allowedExtensions) && !in_array($extension, $this->_allowedExtensions) )
            {
                $errors[] = sprintf($this->_errors[3], implode(', ', $this->_allowedExtensions));
            }
            if( empty($errors) )
            {
                if( in_array($extension, $this->_imageExtensions) && $this->imageWidth !== null )
                {
                    $this->saveImage($filePath, $i);
                }
                else
                {
                    $this->save($filePath, $i);
                }
            }            
            $info = array(
                'hash' => hash_file('md5', $filePath),
                'path' => $this->_relativeDir . $basename,
                'basename' => $basename,
                'filename' => $filename,
                'extension' => $extension,
                'size' => $size,
                'errors' => $errors
            );
            if( in_array($extension, $this->_imageExtensions) )
            {
                $imageInfo = getimagesize($filePath);
                $info['image'] = array(
                    'width' => $imageInfo[0],
                    'height' => $imageInfo[1],
                    'type' => $imageInfo[2],
                    'mime' => $imageInfo['mime']
                );
            }
            return $info;
        }
    
        /**
        * рекурсивное создание директории
        * @param string $dir
        */
        
        public function prepareDir($dir)
        {
            if( !is_dir($dir) )
            {
                if( !mkdir($dir, 0777, true) )
                {
                    throw new Exception("Dir ({$dir}) not found");
                }
            }
            if( !is_writable($dir) )
            {
                if( !@chmod($dir, 0777) )
                {
                    throw new Exception("Dir ({$dir}) is not writable");
                }
            }
        }
    
        /**
        * проверяем серверные настройки
        */
        
        protected function checkServerSettings()
        {
            if( $this->toBytes(ini_get('post_max_size')) < $this->_sizeLimit || $this->toBytes(ini_get('upload_max_filesize')) < $this->_sizeLimit )
            {
                $size = max(1, $this->_sizeLimit / 1024 / 1024) . 'M';
                throw new Exception("Increase post_max_size and upload_max_filesize to {$size}");
            }
        }
    
        /**
        * @param string $string
        * @return integer
        */
        
        protected function toBytes($string)
        {
            $val = (integer) $string;
            switch (strtolower(substr($string, -1)))
            {
                case 'g': $val *= 1024;
                case 'm': $val *= 1024;
                case 'k': $val *= 1024;
            }
            return $val;
        }
        
        public function getName()
        {
            if( isset($_FILES[$this->_name]['name']) )
            {
                return $_FILES[$this->_name]['name'];
            }
            else
            {
                throw new Exception('Could not get file name');
            }
        }
    
        public function getSize()
        {
            if( isset($_FILES[$this->_name]['size']) )
            {
                return $_FILES[$this->_name]['size'];
            }
            else
            {
                throw new Exception('Could not get file size');
            }
        }
    
        public function save($path, $n = 1)
        {
            if( !is_array($_FILES[$this->_name]['tmp_name']) )
            {
                $_FILES[$this->_name]['tmp_name'] = array( $_FILES[$this->_name]['tmp_name'] );
            }
            if( !move_uploaded_file( $_FILES[$this->_name]['tmp_name'][$n], $path) )
            {
                throw new Exception("Could not save file to ({$path})");
            }
            return true;
        }
        
        public function saveImage( $filePath, $i )
        {
            $pathinfo = pathinfo( is_array($this->_file['name']) ? $this->_file['name'][$i] : $this->_file['name'] );
            $extension = strtolower( $pathinfo['extension'] );
            $image_temp_src = is_array( $this->_file['tmp_name'] ) ? $this->_file['tmp_name'][$i] : $this->_file['tmp_name'];
            $image_type = is_array($this->_file['type']) ? $this->_file['type'][$i] : $this->_file['type']; //file type, returns "image/png", image/jpeg
            
            switch( strtolower($image_type) )
            {
                case 'image/png':
                    $created_image =  imagecreatefrompng($image_temp_src);
                    break;
                case 'image/gif':
                    $created_image =  imagecreatefromgif($image_temp_src);
                    break;
                case 'image/jpeg':
                case 'image/pjpeg':
                    $created_image = imagecreatefromjpeg($image_temp_src);
                break;
                default:
                    throw new Exception('Unsupported File!'); //output error and exit
            }
            $this->img_ext = $extension;
            list( $cur_width, $cur_height ) = getimagesize( $image_temp_src );
            //Resize image to our Specified Size by calling resizeImage function.
            if( $this->resizeImage( $cur_width, $cur_height, $this->imageWidth, $filePath, $created_image, $image_type ) )
            {
                //echo $this->_relativDir . '/' . $this->_fileName . '.' . $extension;
            }
            else
            {
                throw new Exception ("Ошибка при изменеии размеров изображения");
            }
        }
        
        public function resizeImage( $CurWidth, $CurHeight, $MaxSize, $DestFolder, $src_image, $image_type )
        {
            $src_image_resource = $src_image;
            if( !is_resource($src_image) )
            {
                switch( strtolower($image_type) )
                {
                    case 'png':
                    case 'image/png':
                        $src_image_resource = imagecreatefrompng( $src_image );
                        break;
                    case 'gif':
                    case 'image/gif':
                        $src_image_resource = imagecreatefromgif( $src_image );
                        break;
                    case 'jpg':
                    case 'jpeg':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        $src_image_resource = imagecreatefromjpeg( $src_image );
                        break;
                    default:
                        $src_image_resource = imagecreatefromjpeg( $src_image );
                }
            }
            if($CurWidth <= 0 || $CurHeight <= 0)
            {
                return false;
            }
        
            //Construct a proportional size of new image
            $ImageScale = min( $MaxSize/$CurWidth, $MaxSize/$CurHeight );
            $NewWidth = ceil( $ImageScale * $CurWidth );
            $NewHeight = ceil( $ImageScale * $CurHeight );
        
            if( $CurWidth < $NewWidth || $CurHeight < $NewHeight )
            {
                $NewWidth = $CurWidth;
                $NewHeight = $CurHeight;
            }            
            $NewCanvas = imagecreatetruecolor($NewWidth, $NewHeight);
            $transparent = imagecolorallocatealpha($NewCanvas, 0, 0, 0, 0);
            imagecolortransparent($NewCanvas, $transparent);
                        
            if(imagecopyresampled($NewCanvas, $src_image_resource, 0, 0, 0, 0, $NewWidth, $NewHeight, $CurWidth, $CurHeight))
            {
                switch(strtolower($image_type))
                {
                    case 'png':
                    case 'image/png':
                        imagealphablending( $NewCanvas, true );
                        imagepng( $NewCanvas, $DestFolder );
                        break;
                    case 'gif':
                    case 'image/gif':
                        imagegif( $NewCanvas, $DestFolder );
                        break;
                    case 'jpg':
                    case 'jpeg':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        imagejpeg( $NewCanvas, $DestFolder, $this->imageQuality );
                        break;
                    default:
                        return false;
                }
            //Destroy image, frees up memory
            if(is_resource($NewCanvas)) {imagedestroy($NewCanvas);}
            return true;
            }
        }
        
        public function cropImage( $img_width, $img_height, $crop_width, $crop_height, $offset_x, $offset_y, $dest_folder, $src_image, $image_type )
        {        
            $new_canvas  = imagecreatetruecolor( $crop_width, $crop_height );
            switch( strtolower($image_type) )
            {
                case 'png':
                    $src_image_resource = imagecreatefrompng( $src_image );
                    break;
                case 'gif':
                    $src_image_resource = imagecreatefromgif( $src_image );
                    break;
                case 'jpg':
                case 'jpeg':
                    $src_image_resource = imagecreatefromjpeg( $src_image );
                    break;
                default:
                    $src_image_resource = imagecreatefromjpeg( $src_image );
            }
            
            if( imagecopyresampled( $new_canvas, $src_image_resource, 0, 0, $offset_x, $offset_y, $crop_width, $crop_height, $crop_width, $crop_height ) )
            {
                $src_image = $dest_folder ?: $src_image;
                switch(strtolower($image_type))
                {
                    case 'png':
                    case 'image/png':
                        imagepng( $new_canvas, $src_image );
                        break;
                    case 'gif':
                    case 'image/gif':
                        imagegif( $new_canvas, $src_image );
                        break;
                    case 'jpg':
                    case 'jpeg':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        imagejpeg( $new_canvas, $src_image, $this->imageQuality );
                        break;
                    default:
                        return false;
                }
                //Destroy image, frees up memory
                if( is_resource( $new_canvas ) )
                {
                    imagedestroy($new_canvas);
                }
                return true;
            }
        }
    }
?>