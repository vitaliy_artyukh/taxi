<?
    error_reporting(E_NOTICE ^E_WARNING);
    ini_set('display_errors', 'On');
	error_reporting(E_ALL);
	
    try
	{
		require_once(dirname(__FILE__) . '/config.php');
		Event::st()->trigger(_HEART_, BEAT);
		Datas::st()->out(Heart::st()->rest());
	}
	catch(Exception $e)
	{
		Datas::st()->header('Retry-After', UNAVAILABLE_TIMEOUT);
		$status = $e->getCode() ? $e->getCode() : UNAVAILABLE;
		Datas::st()->out( array(ERR => $status, MSG => $e->getMessage(), FILE_DATA => $e->getFile(), 
            LINE => $e->getLine(), TRACE => $e->getTrace()) );
		Event::st()->trigger(_HEART_, SANK);
	}
?>