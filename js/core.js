
//start application
$(window).ready(function() {
    Heart.init({
        root : '/'
    });
});

Core = {
    
    param : null,
    _List : '.tm-table-files',
    _Files : null,
    timer : null,
    
    init : function()
    {
        var self = this;
        self._events();
    },
    
    _events : function()
    {
        var self = this;
        
        $(function()
        {
            //console.log('start');
            setInterval(function()
            {
                //console.log('setInterval');
                //self.updateServerTime();
            }, 1000);
            self.updateServerTime(self.param.time);
    
            var progressbar = $(".tm-progress-main"),
                bar = progressbar.find('.uk-progress-bar'),
                settings = {
                    action: '/api/upload/upload', // upload url
                    allow : '*.('+self.param.allow_extension.join('|')+')', // allow extension
                    maxsize: 1048576 * self.param.max_upload_size,
                    loadstart: function() {
                        bar.css("width", "0%");
                        progressbar.removeClass("uk-hidden");
                    },
                    progress: function(percent) {
                        percent = Math.ceil(percent);
                        bar.css("width", percent+"%");
                    },
                    error: function(res) {
                        $.UIkit.notify(res.message, {status: 'danger'});
                    },
                    allcomplete: function(res) {
                        bar.css("width", "100%");
                        setTimeout(function(){
                            progressbar.addClass("uk-hidden");
                        }, 250);
                        
                        if(res.error == 200 || res.error == 0)
                        {
                            $(self._List).find('tbody').prepend(self.drawFileItem(res.data));
                            $.UIkit.notify(res.message, {status: 'success'});
                        }
                        else
                        {
                            $.UIkit.notify(res.message, {status: 'danger'});
                        }
                    }
                };
    
            var select = UIkit.uploadSelect($("#upload-select"), settings),
                drop = UIkit.uploadDrop($("#upload-drop"), settings);
        });
        
        // удаление файла
        $(document).on('click', '.ui-action-delete', function()
        {
            var item = $(this).parents('tr');
                data = {id: item.data('id')};

            self.deleteItem(item, 'Upload', 'deleteFile', data, function(res){
                //
            });
        });
        
        // изменение часового пояса
        $(document).on('change', '.timezone', function()
        {
            var select = $(this),
                data = {timezone: select.val()};
            self.postItem('Upload', 'putTimezome', data, function(res){
                $('#locale').text(select.val());
                clearInterval(self.timer);
                self.param.time = res.time;
                self.updateServerTime(res.time);
            });
        });
    },
    
    // генерация списка файлов
    drawFileList : function(data)
    {
        var self = this, 
            html = '';

        for(item in data)
        {
            if(typeof data[item] === 'object')
            {
                html += self.drawFileItem(data[item]);
            }
        }
        $(self._List).find('tbody').html(html);
    },
    
    // генерация одного файла
    drawFileItem : function(data)
    {
        if(data === undefined)
        {
            console.error('data is undefined');
            return false;
        }
        var id = data.id,
            name = data.name,
            extension = data.extension,
            size = data.size,
            path = data.path,
            date_create = data.date_create,            
            html = '<tr data-id="'+id+'"><td style="width: 1%;"><img width="28" src="img/file_type/'+extension+'.png"></td><td>'+name+'</td><td>'+size+'</td><td>'+date_create+'</td><td class="uk-text-right"><a href="'+path+'" title="Скачать" data-uk-tooltip="" class="uk-margin-mini-right"><i class="uk-icon-cloud-download uk-icon-mini"></i></a><a data-uk-tooltip="" title="Удалить" class="ui-action-delete"><i class="uk-icon-trash-o uk-icon-mini"></i></a></td></tr>';
        return html;
    },
    
    // rest wrapper method POST
    postItem: function(class_name, method, data, callback, notify)
    {
        var method = method === undefined ? 'postItem' : method,
            notify = notify === undefined ? true : notify;
        Heart.rest(class_name + '->' + method, data, function(res)
        {
            if (res.error === 200 || res.error === 0)
            {         
                notify ? $.UIkit.notify(res.message, {status: 'success'}) : null;
                if(callback !== undefined )
                {
                    callback(res);
                }
            }
            else
            {
                $.UIkit.notify(res.message, {status: 'danger'});
            }
        });
    },
    
    // rest wrapper for deleting object
    deleteItem : function(item, class_name, method, data, callback, ask)
    {
        var ask = ask === undefined ? 'Вы действительно хотите удалить этот обьект?' : ask;
        UIkit.modal.confirm(ask, function(){
            var d = {id: item.data('id')};
            data = $.extend(d, data);
            Heart.rest(class_name + '->' + method, data, function(res)
            {
                if (res.error == 200 || res.error == 0)
                {
                    $.UIkit.notify(res.message, {status: 'success'});
                    item.remove();
                    if(callback !== undefined )
                    {
                        callback(res);
                    }
                }
                else
                {
                    $.UIkit.notify(res.message, {status: 'danger'});
                }
            });
        }, {
            center: true
        });
    },
    
    /**
    * TIMER
    **/
    
    updateServerTime : function(time)
    {
        var self = this;
        var ServerTime = new Date(parseInt(time) * 1000);      
        self.timer = setInterval(function()
        {
            ServerTime = new Date(ServerTime.getTime() + 1000),
            day = ServerTime.getUTCDay() < 10 ? '0'+ServerTime.getUTCDay() : ServerTime.getUTCDay(),
            month = ServerTime.getUTCMonth() < 10 ? '0'+ServerTime.getUTCMonth() : ServerTime.getUTCMonth(),
            year = ServerTime.getUTCFullYear(),
            hour = ServerTime.getUTCHours() < 10 ? '0'+ServerTime.getUTCHours() : ServerTime.getUTCHours(),
            minutes = ServerTime.getUTCMinutes() < 10 ? '0'+ServerTime.getUTCMinutes() : ServerTime.getUTCMinutes(),
            second = ServerTime.getUTCSeconds() < 10 ? '0'+ServerTime.getUTCSeconds() : ServerTime.getUTCSeconds();
            document.getElementById('current_time').innerHTML = day+'.'+month+'.'+year+' '+hour+':'+minutes+':'+second;                                            
        }, 1000);
    }
};

Core.init();