window.isIE = ('v'=='\v');
window.isHISTORY = !!(window.history && history.pushState);
window.Heart = {
    _langs : {},
    _account : null,
    _config : {
        root : '',
        api : '/api/',
        separator : '<!>',
        lang : 'en',
        languages : ['en', 'ru', 'ua'],
		tags : { //tags for paste interface
			header : '#topmenu',
			lefter : '#lefter',
			righter : '.section_right',
			footer : '#footer'
		},
		localCache : true,
		cacheTTL : 72,
		fade : 0,
        notify : {
            delay : 1000,
            sound : '/sound/notify.mp3'
        },
        emulateHTTP : false
    },
    conf : function(p) {
        if($.exist(this._config, p)) {
            return this._config[p];
        } else {
            return;
		}
    },
    _objects : {},
    extend : function (a, b) {
        if (typeof this._objects[a] == 'undefined') {
            if (!b) {
                return null;
            } else {
                this._objects[a] = b;
                return this._objects[a];
            }
        } else {
            return this._objects[a];
        }
    },
    _face : {
		header : '',
		lefter : [],
		righter : 'login',
		footer : 'footer',
		'default' : 'login',
		access : {
			nologin : ['login']
		}
	},
    _box_params : {},
    _get : {},
    _post : {},
    _offline : false,
    _core : function () {
        var self = this;
		$.ajaxSetup({
			beforeSend : function(xhr, settings) {
				for(var nl in self._config.noloaders) {
					if(settings.url.match(self._config.noloaders[nl][1]) && 
						settings.type.toUpperCase() == self._config.noloaders[nl][0]) {
						return;
					}
				}
				self.trigger('Heart', 'ajaxStart', [true], self);
			},
			global: true
		});
		$(document).ajaxComplete(function(e, xhr, settings) {
			if($.exist(xhr)) {
				if(xhr['status']==0 && xhr['statusText'] && !self._offline) {
					self._offline = new Date().getTime() / 1000;
					self.trigger('Heart', 'offline', [self._offline], self);
				} else if(self._offline && xhr['status']!=0) {
					self.trigger('Heart', 'online', [self._offline], self);
					self._offline = false;
				}
			}
			self.trigger('Heart', 'ajaxComplete', [true], self);
		});
        $.deparam = function(u) {
			if(!$.is('string', u)) {
				return {};
			}
			
            var o = {};
            var para = u.split('&');
            for (var k in para) {
                var kw = para[k].split('=');
                var res;
                if ((res = kw[0].match(/\[(.*?|\0)\]/i))) {
                    kw[0] = kw[0].split(res[0]).join('');
                    if (res[1]) {
                        if (!o[kw[0]]) {
                            o[kw[0]] = {};
                        }
                        o[kw[0]][res[1]] = kw[1];
                    } else {
                        if (!o[kw[0]]) {
                            o[kw[0]] = [];
                        }
                        o[kw[0]].push(kw[1]);
                    }
                } else {
                    o[kw[0]] = kw[1];
                }
            }
            return o;
        };
		
		$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
            if (!options.localCache || !self.conf('localCache')) {
                return;
            }
            var hourstl = options.cacheTTL || 5;
            var cacheKey = escape(options.cacheKey || options.url.replace(/jQuery./, ''));
            if (options.isCacheValid && !options.isCacheValid()) {
                self.storage.remove(cacheKey);
            }
            var ttl = self.storage.get(cacheKey + '_cachettl');
            if (ttl && ttl < +new Date()) {
                self.storage.remove(cacheKey);
                self.storage.remove(cacheKey + '_cachettl');
                ttl = 'expired';
            }
            var value = self.storage.get(cacheKey);
            if (value) {
                $.log('GET ', 'file://' + cacheKey);
                if (value.indexOf('{') === 0) {
                    value = JSON.parse(value);
                }
                if ($.exist(options, 'dataType') && options.dataType.indexOf('script') === 0) {
                    $.eval(value, options.url);
                }
                if(typeof(options.success)=='function') {
                    options.success(value);
                }
                if(typeof(options.complete)=='function') {
                    options.complete(jqXHR);
                }
                jqXHR.abort();
            } else {
                if (options.success) {
                    options.realsuccess = options.success;
                }
                if (options.complete) {
                    options.realcomplete = options.complete;
                }
                options.success = function (data, bb,cc) {
                    var strdata = data;
                    if ($.exist(this, 'dataType') && this.dataType.indexOf('json') === 0) {
                        strdata = JSON.stringify(data);
                    }
                    if ($.exist(this, 'dataType') && this.dataType.indexOf('script') === 0) {
                        $.eval(data, options.url);
                    }
                    self.storage.set(cacheKey, strdata);
                    if (options.realsuccess)
                        options.realsuccess(data, bb, cc);
                    if (options.realcomplete)
                        options.realcomplete(jqXHR);
                };
                if (!ttl || ttl === 'expired') {
                    self.storage.set(cacheKey + '_cachettl', +new Date() + 1000 * 60 * 60 * hourstl);
                }
            }
			$(document).trigger('ajaxComplete');
        });
        
        $.fn.exists = function()
        {
           return $(this).length;
        };
        
        $.fn.serializeObject = function()
        {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
               if (o[this.name]) {
                   if (!o[this.name].push) {
                       o[this.name] = [o[this.name]];
                   }
                   o[this.name].push(this.value || '');
               } else {
                   o[this.name] = this.value || '';
               }
            });
            //o.formSubmit = '#'+this.attr('id');
            return o;
        };
        
        // получение обьекта с данными при загрузке странице (если нужно)
        /*
        self.rest('Face->getFlush', {first : true}, function()
        {
			$(window).trigger('hashchange', [true]);
			self.trigger('Heart', 'ready', [true]);
        });
        */
        $(window).trigger('hashchange', [true]);
		self.trigger('Heart', 'ready', [true]);
                
    },
    _events : {},
    trigger : function (obj, evt, params, self) {
        if ((typeof(this._events[obj]) == 'undefined') ||
            (typeof(this._events[obj][evt]) == 'undefined') ||
            (!this._events[obj][evt].length) ) {
            return this;
        }
        for (var func in this._events[obj][evt]) {
            this._events[obj][evt][func].apply(self || this._events[obj][evt][func], params || []);
        }
        return this;
    },
    bind : function (obj, evt, func) {
        if (typeof(this._events[obj]) == 'undefined') {
            this._events[obj] = {};
        }
        if (typeof(this._events[obj][evt]) == 'undefined') {
            this._events[obj][evt] = new Array();
        }
        this._events[obj][evt].push(func);
        return this;
    },
    unbind : function(obj, evt) {
        if(obj && evt) {
            if($.exist(this._events, obj) && $.exist(this._events, obj, evt))
                    delete this._events[obj][evt];
        } else if(obj) {
            if($.exist(this._events, obj))
                delete this._events[obj];
        }
        return this;
    },
    rebind : function(obj, evt, func) {
        return this.unbind(obj, evt).bind(obj, evt, func);
    },
    go : function(page, get, post) {
		if(!page) {
			page = this._face['default'];
		}
        var force = (true===get);
        this._get = $.param(get || {});
        this._post[page] = post;
        document.location.href = page;
        return this;
	},
	rest : function(method, data, callback, cache)
    {
		this._last_rest_query = new Date().getTime();
        var parts = method.match(/^(.*?)(\-\>|\:\:|\.|\/)(get|post|put|delete)?(.*?)$/i);
        if(parts) {
            var method = (parts[1] + '/' + parts[4]).toLowerCase();
            var type = $.exist(parts[3]) ? parts[3] : 'get';
        } else {
            var type = 'post';
        }
        callback = $.is('function', callback) ? callback : ($.is('function', data) ? data : function(){});
        data = $.is('function', data) ? {} : data;
        data.PATH_INFO = document.location.pathname;
        var self = this;
        if($.is('string', this._config.emulateHTTP)) {
            if($.is('object', data)) {
                data[this._config.emulateHTTP] = type;
            } else if ($.is('string', data)) {
                data+= '&' + this._config.emulateHTTP + '=' + type;
            }
        }
        $.ajax({
            url: self._config.api + method,
            cache: (cache ? true : false),
            type: type,
            dataType: 'json',
            data: data,
            beforeSend: function(res)
            {
                if(data.formSubmit)
                {
                    $(data.formSubmit).find('input[type="submit"]').attr('disabled', '');
                }
                Heart.loader(true);
            },
            success: function(r, t, a)
            {
				self.ajaxCallback(r, a.status, callback, this);
            },
            complete: function(a, b)
            {
                if(b != 'success') {
                    var r = a.responseText ? $.jencode(a.responseText) : {error: a.status, message: null};
					self.ajaxCallback(r, a.status, callback, this);
                }
                if( data.formSubmit )
                {
                    $(data.formSubmit).find('input[type="submit"]').removeAttr('disabled');
                }
                Heart.loader(false);
            }
        });
	},
    
	url: function(method) {
		var parts = method.match(/^(.*?)(\-\>|\:\:|\.|\/)(get|post|put|delete)?(.*?)$/i);
        var method = (parts[1] + '/' + parts[4]).toLowerCase();
		return this._config.api + method;
	},
    
	ajaxCallback: function(r, status, callback, xhr) {
        var self = this;
        if($.is('string', r)) {
			r = $.jencode(r);
		}		
		if($.is('object', r, 'interface'))
        {
			self._interface(r['interface']);
		}
		if($.is('object', r, 'notify'))
        {
            self._notify(r['notify']);
		}
		if($.is('object', r, 'debug'))
        {
			self._debug(r['debug']);
		}
		if($.is('object', r, 'account') )
        {
			Heart._account = r.account;
			//self.notifyListenter();
		}
		if($.exist(r, 'error') && $.exist(r, 'message') && $.is('string', r.message))
        {
			/*
            self.dialog({title: 'Lang_Error', text: $.getLang(r.message), close : function() {
                if($.is('array', r, 'fields')) {
                    $('input[name="' + r.fields[0] + '"]').focus();
                }
            }});
            */
		}
        if($.is('string', r, 'field'))
        {
            $('[name]').removeClass('uk-form-danger');
            if( $('[name="' + r.field + '"]')[0] )
            {
                $('[name="' + r.field + '"]').addClass('uk-form-danger').focus();
                $('html,body').animate({scrollTop : $('[name="' + r.field + '"]').position().top - 120}, 750);
            }
            else
            {
                console.error('input name: '+r.field+' not found');
            }
        }
		if($.is('function', callback))
        {
			callback(r);
		}
	},
    
    _debug: function(d) {
        if('v'=='\v') {
            return false;
        }
		console.trace = $.trace;

        if(typeof(console) != 'undefined')
            for(x in d) //m = method, a = arguments
                if(typeof(console[d[x]['m']]) != 'undefined')
                    if(typeof(console[d[x]['m']].apply)=='function')
						try {
							console[d[x]['m']].apply(console[d[x]['m']], d[x]['a']);
						} catch(e) {}
    },
    
    init : function (config) {
        this._config = $.extend(this._config, config);
        this._core();
    },
    
    reboot: function(force, nocache) {
        if(force===true) {
            document.location.reload(nocache);
        } else {
            this.init(this._config);
        }
    },
    
    hash: function() {
        return document.location.hash.substr(1);
    },
    
    // GET параметры адресной строки
    _GET : function(key)
    {
        var parts = window.location.search.substr(1).split("&");
        var $_GET = {};
        for (var i = 0; i < parts.length; i++) {
            var temp = parts[i].split("=");
            $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
        }
        if(key === undefined)
        {
            return $_GET
        }
        else
        {
            return $_GET[key];
        }
    },    
	fs: function (el)
    {
		el = el || document.body;
		var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || 
			el.mozRequestFullScreen || el.msRequestFullScreen;
		if (requestMethod) {
			requestMethod.call(el);
		} else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
			var wscript = new ActiveXObject("WScript.Shell");
			if (wscript !== null) {
				wscript.SendKeys("{F11}");
			}
		}
		return this;
	},
    loader : function(show)
    {
        if(show) {
            $('#loader').show();
        } else {
            $('#loader').hide();
        }
    },
    date : {
        format : function(format, date)
        {
            var date = date == undefined ? new Date() : date;
            switch( typeof date )
            {
                case 'string' :
                    var date = date.split(/[- :]/),
                    date = new Date(date[0], date[1]-1, date[2], date[3], date[4], date[5]);
                break;
                
                default :
                    var date = date;
                break;                                                
            }
            
            var yyyy = date.getFullYear(),
                yy = yyyy.toString().substring(2),
                m = date.getMonth() + 1,
                mm = m < 10 ? "0" + m : m,
                //mmm = $.getLang('Lang_month_'+m).substring(0, 3),
                mmmm = date.toLocaleFormat("%B"),
                d = date.getDate(),
                dd = d < 10 ? "0" + d : d,
                //wwww = $.getLang('Lang_weekday_'+date.getDay()),
                h = date.getHours(),
                hh = h < 10 ? "0" + h : h,
                i = date.getMinutes(),
                ii = i < 10 ? "0" + i : i,
                s = date.getSeconds(),
                ss = s < 10 ? "0" + s : s;
                
            format = format.replace(/yyyy/gi, yyyy);
    		format = format.replace(/yy/gi, yy);
            format = format.replace(/mmmm/gi, mmmm);
            //format = format.replace(/mmm/gi, mmm);
            format = format.replace(/mm/gi, mm);
            format = format.replace(/m/gi, m);
            format = format.replace(/dd/gi, dd);
            format = format.replace(/d/gi, d);
            //format = format.replace(/wwww/gi, wwww);
    		format = format.replace(/hh/gi, hh);
    		format = format.replace(/h/gi, h);
            format = format.replace(/ii/gi, ii);
            format = format.replace(/i/gi, i);
            format = format.replace(/ss/gi, ss);
            format = format.replace(/s/gi, s);
            return format;
        },
    },
    history : {
        push : function(url, one, two) {
            one = one || {};
            two = two || {};
            history.pushState(one, two, url);
        }
    },
    storage: {
        _storage : {},
        _check : function () {
            return ((typeof(window['localStorage']) == 'object') &&
                (typeof(window['localStorage']['setItem']) == 'function'));
        },
        set : function (key, val, type) {
            type = type || 'local';
            if(!key) {
                return this;
            }
            val = val || null;
            if (this._check()) {
                window[type + 'Storage'].setItem(key, this._enc(val));
            } else {
                this._storage[key] = val;
            }
            this._callback();
            return this;
        },
        isset : function(key, type) {
            if (this.get(key, type)) {
                return true;
            }
            return false;
        },
        get : function (key, type) {
            type = type || 'local';
            if(!key) {
                return undefined;
            }
            if (this._check()) {
                return this._dec(window[type + 'Storage'].getItem(key));
            } else {
                return this._storage[key] || null;
            }
        },
        search : function (exp, type) { /* ^u([0-9]{1,11})$ */
            type = type || 'local';
            var results = {};
            exp = exp || '^.*?$';
            exp = new RegExp(exp, 'i');

            if (this._check()) {
                for (var i = 0; i < window[type + 'Storage'].length; i++) {
                    var tmpk = window[type + 'Storage'].key(i);
                    if(String(tmpk).match(exp)) {
                        results[tmpk] = this._dec(window[type + 'Storage'].getItem(tmpk));
                    }
                }
            } else {
                for (var key in this._storage) {
                    if(String(key).match(exp)) {
                        results[key] = this._storage[key];
                    }
                }
            }
            return results;
        },
        size : function (exp, type) { /* ^u([0-9]{1,11})$ */
            type = type || 'local';
            var len = 0;
            exp = exp || '^.*?$';
            exp = new RegExp(exp, 'i');

            if (this._check()) {
                for (var i = 0; i < window[type + 'Storage'].length; i++) {
                    var tmpk = window[type + 'Storage'].key(i);
                    if(String(tmpk).match(exp)) {
                        len+= window[type + 'Storage'].getItem(tmpk).length;
                    }
                }
            } else {
                for (var key in this._storage) {
                    if(String(key).match(exp)) {
                        len+= this._storage[key].length;
                    }
                }
            }
            return len;
        },
        clear : function (type) {
            type = type || 'local';
            window[type + 'Storage'].clear();
            this._storage = {};
            this._callback();
            return this;
        },
        list : function (type) {
            type = type || 'local';
            var keys = Array();
            if (this._check()) {
                for (var i = 0; i < window[type + 'Storage'].length; i++) {
                    keys.push(window[type + 'Storage'].key(i));
                }
            } else {
                for (var key in this._storage) {
                    keys.push(key);
                }
            }
            return keys;
        },
        remove : function (key, type) {
            type = type || 'local';
            if (this._check()) {
                window[type + 'Storage'].removeItem(key);
            } else {
                this._storage[key] = null;
                delete this._storage[key];
            }
            this._callback();
            return this;
        },
        _callback : function(i) {},
        localEvent : function (callback) {
            this._callback = callback;
            return this;
        },
        remoteEvent : function (callback) {
            if ('v' == '\v') { // Note: IE listens on document
                document.attachEvent('onstorage', callback, false);
            } else if (window.opera || window.webkit) { // Note: Opera and WebKits listens on window
                window.addEventListener('storage', callback, false);
            } else { // Note: FF listens on document.body or document
                document.body.addEventListener('storage', callback, false);
            }
            return this;
        },
        _enc : function(d) {
            return d;
            return JSON.stringify(d);
        },
        _dec : function(e) {
            return e;
            if($.exist(e)) {
                return JSON.parse(e);
            } else {
                return null;
            }
        }
    },
    cookie : function (key, value, options) {
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || 
                value === null || value === undefined)) {
            options = options || {};
            if (value === null || value === undefined) {
                options.expires = -1;
            }
            if (typeof options.expires === 'number') {
                var days = options.expires,
                t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }
            value = String(value);
            return (document.cookie = [
                    encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
                    options.expires ? '; expires=' + options.expires.toUTCString() : '',
                    options.path ? '; path=' + options.path : '',
                    options.domain ? '; domain=' + options.domain : '',
                    options.secure ? '; secure' : ''
                ].join(''));
        }
        options = value || {};
        var decode = options.raw ? function (s) { return s; } : decodeURIComponent;
        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            if (decode(pair[0]) === key)
                return decode(pair[1] || '');
        }
        return null;
    },
	_temporary : {},
	temp : function(key, val, ttl) {
		if(!$.exist(key)) {
			return false;
		}
		
		if($.is('object', key)) {
			key = $.param(key);
		}
		
		if($.exist(val)) {
			ttl = ttl || 3600;
			this._temporary[key + '_ttl'] = (new Date().getTime() / 1000) + ttl;
			this._temporary[key] = val;
			return this;
		} else {
			if($.exist(this._temporary, key + '_ttl')) {
				if(this._temporary[key + '_ttl'] < (new Date().getTime() / 1000)) {
					delete this._temporary[key + '_ttl'];
					delete this._temporary[key];
				}
			}
			
			if($.exist(this._temporary, key)) {
				return this._temporary[key];
			} else {
				return false;
			}
		}
	},
    
    _notifier: {},
    _notify: function(all)
    {
        var self = this;
        console.log('NOTIFY');
	},
	_last_rest_query : 0,
	_notify_delay : 60,
	_notify_timeout : null,
    
    notifyListenter: function()
    {
        clearTimeout(this._notify_timeout);
		if(!this._account) {
			return false;
		}
        var self = this;                        
		if(new Date().getTime() - self._notify_delay * 1000 > self._last_rest_query)
        {
			this._notify_timeout = setTimeout(function() {
				self.rest('Notify->getPing', {}, function() {
					self.notifyListenter();
				});
			}, self._notify_delay * 1000);
		} else {
			this._notify_timeout = setTimeout(function() {
				self.notifyListenter();
			}, self._notify_delay * 1000);
		}
	},
    
    /**
     * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
     * @param  iNumber Integer Число на основе которого нужно сформировать окончание
     * @param  aEndings Array Массив слов или окончаний для чисел (1, 4, 5),
     *         например ['яблоко', 'яблока', 'яблок']
     * @return String
     */
     
    getNumEnding : function(iNumber, aEndings)
    {
        var sEnding, i;
        iNumber = iNumber % 100;
        if (iNumber>=11 && iNumber<=19) {
            sEnding=aEndings[2];
        }
        else {
            i = iNumber % 10;
            switch (i)
            {
                case (1): sEnding = aEndings[0]; break;
                case (2):
                case (3):
                case (4): sEnding = aEndings[1]; break;
                default: sEnding = aEndings[2];
            }
        }
        return sEnding;
    },
    
    serialize : function(obj)
    {
        var str = [];
        for(var p in obj)
        if (obj.hasOwnProperty(p))
        {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
        return str.join("&");
    }
}

Array.prototype.unset = function(val)
{
    if(typeof this === 'object')
    {
        var index = this.indexOf(val);
        if(index > -1)
        {
            this.splice(index, 1);
        }
    }
};

Array.prototype.in_array = function(val)
{
	for(var i = 0, l = this.length; i < l; i++)
    {
		if(this[i] == val)
        {
			return true;
		}
	}
	return false;
};

Array.prototype.array_search = function(key, val)
{
	for(var item in this)
    {
        if(this[item][key] == val)
        {
            return this[item];
        }
    }
    return false;
};

String.prototype.is_numeric = function(mixed_var) {
     return (typeof mixed_var === 'number' || typeof mixed_var === 'string') 
        && mixed_var !== '' && !isNaN(mixed_var);
};

$.exist = function(o) {
    if((typeof(o) == 'undefined') || (!o)) {
        return false;
    }
    for (var i = 1; i < arguments.length; i++) {
        if ((typeof(o[arguments[i]]) == 'undefined') || (!o[arguments[i]])) {
            return false;
        } else {
            o = o[arguments[i]];
        }
    }
    return true;
};
$.del = function(c) {
	if($.exist.apply($.exist, arguments)) {
		for(var i in arguments) {
			c = c[i];
		}
		delete c;
		return true;
	}
	return false;
};
$.is = function(t, o) {
    if((typeof(o) == 'undefined') || (!o)) {
        return false;
    }
    for (var i = 2; i < arguments.length; i++) {
        if ((typeof(o[arguments[i]]) == 'undefined') || (!o[arguments[i]])) {
            return false;
        } else {
            o = o[arguments[i]];
        }
    }
    t = t || 'string';
    if(t=='a' || t=='array') {
        if((typeof(o)=='object') && (o instanceof Array)) {
            return true;
        } else {
            return false;
        }
    } else {
        t = (t=='s'?'string':(t=='o'?'object':(t=='n'?'number':(t=='b'?'boolean':t))));
    }
    if(typeof(o) == t) {
        return true;
    } else {
        return false;
    }
};

$.eval = function (c, f) {
	try {
		if('{' !== c.substr(0, 1)) {
			eval(c);
		}
	} catch (e) {
		var er = e.constructor('Evaluate Error: ' + e.message);
		er.lineNumber = e.lineNumber - er.lineNumber + 3;
		er.fileName = window.document.baseURI + f;
		throw (er);
		//$.log(er);
	}
};

$.jencode = function(j) {
	try {
		var tmp = eval('(' + j + ')');
		delete tmp;
		return $.parseJSON(j);
	} catch(e) {
		return null;
	}
};
$.wait = function(wait, callback, ctx) {
    if(typeof ctx == 'function') {
        return setTimeout(function() { ctx.call(callback); }, wait);
    } else {
        return setTimeout(callback, wait);
    }
};
$.prototype.rebind = function(e, f) { return $(this).unbind(e).bind(e, f); };
$.log=function(){if($.is('function',window,'console','log','apply'))try{console.log.apply(console.log,arguments)}catch(e){}};
$.info=function(){if($.is('function',window,'console','info','apply'))try{console.info.apply(console.info,arguments)}catch(e){}};
$.warn=function(){if($.is('function',window,'console','warn','apply'))try{console.warn.apply(console.warn,arguments)}catch(e){}};
$.error=function(){if($.is('function',window,'console','error','apply'))try{console.error.apply(console.error,arguments)}catch(e){}};
$.exception=function(){if($.is('function',window,'console','exception','apply'))try{console.exception.apply(console.exception,arguments)}catch(e){}};
$.trace = function (name, data) {
	if($.exist(window, 'console', 'dir') &&
		$.exist(window, 'console', 'groupCollapsed') &&
		$.exist(window, 'console', 'groupEnd')) {
		console.groupCollapsed(name);
		console.dir(data);
		console.groupEnd(name);
	}
};

Number.prototype.pZ = function(len){
    var s = String(this), c = '0';
    len = len || 2;
    while(s.length < len) s = c + s;
        return s;
};
JVN = 'javascript:void(0);';
count = function(o) {var c = 0;for(var i in o) {c++;};return c;};
array_keys = function(o) { var a = []; for(var k in o) {a.push(k);} return a;};
array_values = function(o) { var a = []; for(var k in o) {a.push(o[k]);} return a;};
enty = function(s) { return s ? String(s).replace(/&amp;/g, '&') : ''; };
deenty = function(s) { return s ? str_replace('>', '&gt;', str_replace('<', '&lt;', s)) : ''; };
str_replace = function(from, to, str) { return str.split(from).join(to); };
oReverse = function(o) {
	return o;
	var a = array_keys(o).reverse(), n = {};
	for(var k in a) {
		n[a[k]] = o[a[k]];
	}
	return n;
};
nl2br = function(str) {
	return str.split("\n").join("<br/>").split("\\n").join("<br/>").split(" ").join("&nbsp;");
};
linkify = function (t) {
	if (!t) {
		return t;
	}
	var p = t.split('&nbsp;');
	for(var i in p) {
		p[i] = p[i]
			.replace(/((https?\:\/\/|ftp\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/gi,
			function (url) {
				nice = url;
				if (url.match('^https?:\/\/')) {
					nice = nice.replace(/^https?:\/\//i, '')
				} else
					url = 'http://' + url;
				
				return '<a target="_blank" rel="nofollow" href="' + url + '">' + nice.replace(/^www./i, '') + '</a>';
			});
	}
	return p.join('&nbsp;');
}
next = function(obj, key) {
	var prev = '';
	for(var k in obj) {
		if(prev == key) {
			return k;
		} else {
			prev = k;
		}
	}
	return null;
};
prev = function(obj, key) {
	var prev = null;
	for(var k in obj) {
		if(k == key) {
			return prev;
		} else {
			prev = k;
		}
	}
	return null;
};
first = function(obj) {
	for(var k in obj) {
		return k;
	}
};
last = function(obj) {
	for(var k in obj) {};
	return k;
};
define = function(k, v) {
	if(!$.exist(window, k)) {
		window[k] = v;
	}
};