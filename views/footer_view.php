<!--FOOTER_START-->
<div class="tm-footer-wrap">
    <div class="uk-container uk-container-center">
        <div class="uk-text-center uk-padding-medium-top">
            <div><a href="/"><img src="./img/logo.png" width="80" /></a></div>
            <ul class="uk-subnav uk-subnav-dott">
                <li><a href="https://bitbucket.org/vitaliy_artyukh/taxi">Bitbucket project</a></li>
                <li><a href="http://vk.com/vitaliy.artyukh">Контакты</a></li>
            </ul>
            <div class="uk-text-normal uk-color-light">2015 © Vintage</div>
        </div>
    </div>
</div>
<!--FOOTER_END-->