<meta http-equiv="content-type" content="text/html" charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Test taxi</title>

<link rel="icon" href="/img/favicon.ico" type="image/x-icon" />
<link rel="icon" href="/img/favicon.png" type="image/x-icon" />

<link rel="stylesheet" type="text/css" href="/css/uikit.css" />
<link rel="stylesheet" type="text/css" href="/css/uikit-addons.css" />
<link rel="stylesheet" type="text/css" href="/css/ui.css" />

<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/uikit/uikit.js"></script>
<script type="text/javascript" src="/js/uikit/uikit-addons-min.js"></script>
<script type="text/javascript" src="/js/heart.js"></script>
<script type="text/javascript" src="/js/core.js"></script>