<!DOCTYPE html>
<html>
	<head>
		<!--HEAD_START-->
        <?= $this->includeTpl('head_view');?>
        <!--HEAD_END-->
	</head>
    
	<body>			
		<div>
            <div id="loader"><i class="uk-icon-refresh uk-icon-spin"></i></div>
            <!--HEADER_START-->
            <?= $this->includeTpl('header_view'); ?>
            <!--HEADER_END-->
    		
    		<!--CONTENT_START-->
            <?= $this->includeTpl($this->_content); ?>
            <!--CONTENT_END-->
    		
    		<!--FOOTER_START-->
            <?= $this->includeTpl('footer_view');?>
            <!--FOOTER_END-->
        </div>
	</body>
</html>